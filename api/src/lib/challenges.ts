import { Challenge } from './types/challenges';

export const createdChallengeObject = (challengeId: string, challenges: Challenge[]) => {
    const maxNumberOfChallenges = 3;
    const numberOfChallenges = challenges.length;

    if (numberOfChallenges < maxNumberOfChallenges) {
        const [firstChallenge, secondChallenge] = challenges;
        const { id: firstId } = firstChallenge;
        const { id: secondId } = secondChallenge;

        if (firstId === challengeId) {
            return {
                ...firstChallenge,
                next_challenge: secondId,
                prev_challenge: null,
            };
        }

        return {
            ...secondChallenge,
            next_challenge: null,
            prev_challenge: firstId,
        };
    }

    const [prev, challenge, next] = challenges;
    const { id: prevId } = prev;
    const { id: nextId } = next;

    return { ...challenge, prev_challenge: prevId, next_challenge: nextId };
};
