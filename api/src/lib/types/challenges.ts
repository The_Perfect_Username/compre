export type Challenge = {
    id: string;
    domain_id: string;
    domain?: string;
    challenge: string;
    status: string;
    created: string;
};
