import express from 'express';
import { verifyUser } from '../db/auth';
import { fetchChallengesBySearch } from '../db/challenges';

const router = express();

router.get('/', verifyUser, (req: any, res: any) => {
    const query = req.query || {};

    if (!query.hasOwnProperty('search_query')) {
        return res.status(200).json([]);
    }

    const userId = req.userId;
    const searchTerms = query.search_query || '';
    const limit = query.limit || 10;
    const offset = query.offset || 0;

    fetchChallengesBySearch(searchTerms, userId,  Number(limit), Number(offset))
        .then((response: any) => res.status(response.code).json(response.results))
        .catch((error: any) => res.status(error.code).json({ error: 'Unable to perform search' }));
});

export default router;
