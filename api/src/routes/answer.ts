import axios from 'axios';
import express from 'express';
import { PYTHON_API_URL } from '../config/globals';
import { verifyUser } from '../db/auth';
import { getModelByChallengeId } from '../db/models';
import {
    calculateStatus,
    createAnswer,
    getAnswer,
} from '../db/user_answers';

const router = express();

router.get('/', verifyUser, (req: any, res: any) => {
    const query = req.query || {};

    if (!query.hasOwnProperty('id')) {
        return res.sendStatus(204);
    }

    const userId = req.userId;
    const challengeId = query.id;

    getAnswer(userId, challengeId)
        .then((response: any) => {
            const { results } = response;
            res.status(response.code).json(results);
        })
        .catch((error: any) => res.status(error.code).json({ error: error.message }));
});

const score = (modelId: string, answer: string) => {
    return new Promise((resolve, reject) => {
        axios.put(`${PYTHON_API_URL}/score/${modelId}`, { answer })
            .then((response: any) => resolve(response.data))
            .catch((error: any) => reject(error));
    });
};

router.post('/', verifyUser, (req: any, res: any) => {
    const payload = req.body || {};
    const payloadIsEmpty = Object.keys(payload).length === 0;

    if (payloadIsEmpty) {
        return res.status(422).json({ error: 'Request payload is missing or empty' });
    }

    const userId = req.userId;
    const challengeId = payload.challenge_id || '';
    const answer = payload.answer || '';

    if (answer.trim().length === 0) {
        return res.status(422).json({ error: 'Answer is missing' });
    } else if (answer.length > 1000) {
        return res.status(422).json({ error: 'Answer must not exceed 1000 characters' });
    }

    getModelByChallengeId(challengeId)
        .then((model: any) => score(model.id, answer))
        .then((answerScore: any) => {
            const answerScoreAsFloat = parseFloat(answerScore);
            const status = calculateStatus(answerScoreAsFloat);

            createAnswer(userId, challengeId, answer, answerScoreAsFloat, status)
                .then(() => res.status(200).json({ status }))
                .catch((error: any) => res.status(error.code).json({ error: error.message }));
        })
        .catch((error: any) => res.status(500).json({ error: error.message }));
});

export default router;
