import express from 'express';
import fs from 'fs';
import { MODEL_BASE_DIRECTORY } from '../config/globals';
import {
    adminOnly,
    verifyUser,
} from '../db/auth';
import {
    createChallenge,
    createContestion,
    fetchChallengesForUser,
    fetchDomainChallenges,
    fetchDomainChallengesByTerms,
    getChallenge,
} from '../db/challenges';
import {
    fetchDomains,
    updateDomainStatus,
} from '../db/domain';
import {
    createModel,
    deleteModelByChallengeId,
    getModelByChallengeId,
    moveModelFile,
} from '../db/models';

const router = express();

const acceptableFileTypes: string[] = ['application/octet-stream'];

router.get('/', verifyUser, (req, res) => {
    const query = req.query || {};
    const queryIsEmpty: boolean = Object.keys(query).length === 0;

    if (queryIsEmpty) {
        return res.status(422).json({ error: 'Missing parameters' });
    }

    const challengeId = query.id || '';

    if (challengeId.length === 0) {
        return res.sendStatus(204);
    }

    getChallenge(challengeId)
        .then((response: any) => res.status(response.code).json(response.results))
        .catch((error: any) => res.status(error.code).json({ error: error.message }));

});

router.get('/fetch', verifyUser, (req: any, res: any) => {
    const query = req.query || {};

    if (!query.hasOwnProperty('id')) {
        return res.sendStatus(204);
    }

    const userId = req.userId;
    const domainId = query.id;
    const limit = query.limit || 10;
    const offset = query.offset || 0;

    fetchChallengesForUser(domainId, userId, Number(limit), Number(offset))
        .then((response: any) => {
            const { code, results } = response;
            res.status(code).json(results);
        })
        .catch((error: any) => res.status(error.code).json({ error: error.message }));
});

router.get('/fetch/:id', verifyUser, (req: any, res: any) => {
    const params = req.params || {};
    const query = req.query || {};

    if (!params.hasOwnProperty('id')) {
        return res.status(200).json([]);
    }

    const domainId = params.id;
    const searchQuery = query.search_query || '';
    const limit = query.limit || 10;
    const offset = query.offset || 0;

    if (searchQuery.length === 0) {
        fetchDomainChallenges(domainId, Number(limit), Number(offset))
            .then((response: any) => {
                const { code, results } = response;
                res.status(code).json(results);
            })
            .catch((error: any) => res.status(error.code).json({ error: error.message }));
    } else {
        fetchDomainChallengesByTerms(domainId, searchQuery, Number(limit), Number(offset))
            .then((response: any) => {
                const { code, results } = response;
                res.status(code).json(results);
            })
            .catch((error: any) => res.status(error.code).json({ error: error.message }));
    }
});

router.post('/', verifyUser, adminOnly, (req: any, res: any) => {
    const payload = req.body || {};

    const payloadIsEmpty = Object.keys(payload).length === 0;

    if (payloadIsEmpty) {
        return res.status(422).json({ error: 'Payload is missing or empty' });
    }

    const userId = req.userId;
    const domain: string = payload.domain || '';
    const challenge: string = payload.challenge || '';
    const file: any = (req.files && req.files.file) || null;

    const error: ChallengeErrorResponse = {};

    if (domain === '') {
        error.domain = 'Domain is missing';
    }

    if (challenge.trim().length === 0) {
        error.challenge = 'Challenge must not be left blank';
    } else if (challenge.length > 100) {
        error.challenge = 'Challenge must not exceed 100 characters';
    }

    if (!file) {
        error.file = 'File is missing';
    } else if (file && !acceptableFileTypes.includes(file.mimetype)) {
        error.file = 'File format must be joblib';
    }

    const hasErrors = Object.keys(error).length > 0;

    if (hasErrors) {
        return res.status(422).json(error);
    }

    createChallenge(challenge, userId, domain)
        .then((response: any) => {
            const { id } = response;

            createModel(id)
                .then((model: any) => moveModelFile(file, model.id))
                .then((model: any) => {
                    updateDomainStatus('modified', domain)
                        .then(() => res.status(model.code).json({ id }))
                        .catch((err: any) => res.status(err.code).json({ error: err.message }));
                })
                .catch((err: any) => res.status(err.code).json({ error: err.message }));
        })
        .catch((err: any) => res.status(err.code).json({ error: err.message }));
});

router.put('/:id/model', verifyUser, adminOnly, (req, res) => {
    const params = req.params || {};

    if (!params.hasOwnProperty('id')) {
        return res.status(422).json({ error: 'Missing ID' });
    }

    const challengeId = params.id;
    const file: any = (req.files && req.files.file) || null;

    const error: UpdateModelErrorResponse = {};

    if (!file) {
        error.file = 'File is missing';
    } else if (file && !acceptableFileTypes.includes(file.mimetype)) {
        error.file = 'File format must be joblib';
    }

    const hasErrors = Object.keys(error).length > 0;

    if (hasErrors) {
        return res.status(422).json(error);
    }

    getModelByChallengeId(challengeId)
        .then((model: any) => {
            const { id } = model;

            if (!id) {
                throw {
                    code: 409,
                    message: 'Unable to update model because it does not exist',
                };
            }

            try {
                fs.unlinkSync(`${MODEL_BASE_DIRECTORY}/${id}.joblib`);
            } catch (e) {
                console.error(e);
            }

            return deleteModelByChallengeId(challengeId);
        })
        .then(() => createModel(challengeId))
        .then((model: any) => moveModelFile(file, model.id))
        .then((model: any) => res.status(model.code).json({ id: model.id }))
        .catch((err: any) => res.status(err.code).json({ error: err.message }));
});

router.post('/contest', verifyUser, (req: any, res: any) => {
    const payload = req.body || {};

    if (Object.keys(payload).length === 0) {
        return res.status(422).json({ error: 'Missing parameters' });
    }

    const userId: string = req.userId;
    const answer: string = payload.answer || '';
    const challengeId: string = payload.challenge_id || '';

    if (answer.trim().length === 0) {
        return res.status(422).json({ error: 'Answer must not be left empty' });
    } else if (answer.length > 1000) {
        return res.status(422).json({ error: 'Answer must not exceed 1000 characters' });
    }

    createContestion(challengeId, userId, answer)
        .then(() => res.sendStatus(204))
        .catch(() => res.status(500).json({ error: 'Was unable to submit your contestion' }));

});

router.get('/categories', (_, res) => {
    const limit = 100;
    const offset = 0;
    fetchDomains(limit, offset)
        .then((response: any) => res.status(response.code).json(response.results))
        .catch((err: any) => res.status(err.code).json({ message: err.message }));
});

type ChallengeErrorResponse = {
    domain?: string;
    challenge?: string;
    answer?: string;
    file?: string;
};

type UpdateModelErrorResponse = {
    file?: string;
};

export default router;
