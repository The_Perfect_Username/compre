import express from 'express';
import { login } from '../db/login';

const router = express();

router.post('/', (req, res) => {
    const payload = req.body || {};
    const payloadIsEmpty = Object.keys(payload).length === 0;

    if (payloadIsEmpty) {
        return res.status(422).json({ error: 'Request body is empty or missing' });
    }

    const email = payload.email || '';
    const password = payload.password || '';

    const emailIsMissing = email.trim().length === 0;
    const passwordIsMissing = password.trim().length === 0;

    if (emailIsMissing || passwordIsMissing) {
        return res.status(401).json({ error: 'Email/password combination is incorrect' });
    }

    login(email, password)
        .then((response: any) => {
            const { code, user } = response;
            res.status(code).json(user);
        })
        .catch((error: any) => {
            const { code, message } = error;
            res.status(code).json({ error: message });
        });
});

export default router;
