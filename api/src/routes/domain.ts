import express from 'express';
import {
    adminOnly,
    verifyUser,
} from '../db/auth';
import {
    publishChallenges,
} from '../db/challenges';
import {
    createDomain,
    fetchDomains,
    fetchDomainsByTerms,
    getDomain,
    updateDomainName,
    updateDomainStatus,
} from '../db/domain';

const router = express();

router.get('/fetch', verifyUser, (req: any, res: any) => {
    const query = req.query || {};
    const searchQuery = query.search_query || '';
    const limit = query.limit || 10;
    const offset = query.offset || 0;

    if (searchQuery.length === 0) {
        fetchDomains(Number(limit), Number(offset))
            .then((response: any) => {
                const { code, results } = response;
                res.status(code).json(results);
            })
            .catch((error: any) => res.status(error.code).json({ error: error.message }));
    } else {
        fetchDomainsByTerms(searchQuery, Number(limit), Number(offset))
            .then((response: any) => {
                const { code, results } = response;
                res.status(code).json(results);
            })
            .catch((error: any) => res.status(error.code).json({ error: error.message }));
    }
});

router.get('/:id', verifyUser, (req: any, res: any) => {
    const params = req.params || {};

    if (!params.hasOwnProperty('id')) {
        return res.status(422).json({ error: 'Missing domain parameter' });
    }

    const { id } = params;

    getDomain(id)
        .then((response: any) => res.status(response.code).json(response.results))
        .catch((error: any) => res.status(error.code).json({ error: error.message }));
});

router.post('/', verifyUser, adminOnly, (req: any, res: any) => {
    const payload = req.body || {};

    if (!payload.hasOwnProperty('domain')) {
        return res.status(422).json({ error: 'Missing domain parameter' });
    }

    const { domain } = payload;

    if (domain.trim().length === 0) {
        return res.status(422).json({ error: 'Domain name is missing or blank' });
    }

    if (domain.length > 100) {
        return res.status(422).json({ error: 'Domain name must not exceed 100 characters' });
    }

    createDomain(domain)
        .then((response: any) => res.status(200).json({ id: response.id, name: domain }))
        .catch((error: any) => res.status(error.code).json({ error: error.message }));
});

router.put('/publish/:id', verifyUser, adminOnly, (req: any, res: any) => {
    const params = req.params || {};

    if (!params.hasOwnProperty('id')) {
        return res.status(422).json({ error: 'Missing domain id' });
    }

    const { id } = params;

    updateDomainStatus('published', id)
        .then(() => publishChallenges(id))
        .then(() => res.sendStatus(204))
        .catch((error: any) => res.status(error.code).json({ error: error.message }));
});

router.put('/:id', verifyUser, adminOnly, (req: any, res: any) => {
    const params = req.params || {};
    const payload = req.body || {};

    if (!params.hasOwnProperty('id')) {
        return res.status(422).json({ error: 'Missing domain id parameter' });
    }

    if (!payload.hasOwnProperty('name')) {
        return res.status(422).json({ error: 'Missing domain name parameter' });
    }

    const { id } = params;
    const { name } = payload;

    if (name.trim().length === 0) {
        return res.status.json({ error: 'Domain name is missing or blank' });
    }

    if (name.length > 100) {
        return res.status(422).json({ error: 'Domain name must not exceed 100 characters' });
    }

    updateDomainName(name, id)
        .then((response: any) => res.status(200).json({ id: response.id }))
        .catch((error: any) => res.status(error.code).json({ error: error.message }));
});

export default router;
