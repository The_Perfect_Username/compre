import bcrypt from 'bcrypt';
import express from 'express';
import { verifyUser } from '../db/auth';
import {
    createUser,
    getUser,
    updateUser,
    updateUserPassword,
} from '../db/user';

const router = express.Router();

router.post('/', (req, res) => {
    const payload: User = req.body || {};
    const payloadIsEmpty: boolean = Object.keys(payload).length === 0;

    if (payloadIsEmpty) {
        return res.status(422).json({ message: 'Request body is missing or empty' });
    }

    try {
        const name: string = payload.name || '';
        const email: string = payload.email || '';
        const password: string = payload.password || '';

        const error: responseError = {};

        if (name.trim().length === 0) {
            error.name = 'Name must not be left empty';
        } else if (name.length > 50) {
            error.name = 'Name must not exceed 50 characters';
        } else if (!name.match(/^[A-Za-z|\s]+$/g)) {
            error.name = 'Name must only contain alphabetical characters and whitespace';
        }

        if (email.trim().length === 0) {
            error.email = 'Email must not be left empty';
        } else if (email.length > 75) {
            error.email = 'Email must not exceed 75 characters';
        }

        if (password.length < 6) {
            error.password = 'Password must be at least 6 characters';
        } else if (password.includes(' ')) {
            error.password = 'Password must not contain whitespace';
        }

        const hasErrors = Object.keys(error).length > 0;

        if (hasErrors) {
            return res.status(422).json(error);
        }

        createUser(name, email, password)
            .then((response: any) => res.status(response.code).json(response.user))
            .catch((err) => res.status(err.code).json({ error: err.message }));
    } catch (error) {
        res.sendStatus(500);
    }
});

router.put('/', verifyUser, (req: any, res: any) => {
    const payload: User = req.body || {};
    const payloadIsEmpty: boolean = Object.keys(payload).length === 0;

    if (payloadIsEmpty) {
        return res.status(422).json({ message: 'Request body is missing or empty' });
    }

    try {
        const userId = req.userId;
        const name: string = payload.name || '';
        const email: string = payload.email || '';

        const error: responseError = {};

        if (name.trim().length === 0) {
            error.name = 'Name must not be left empty';
        } else if (name.length > 50) {
            error.name = 'Name must not exceed 50 characters';
        } else if (!name.match(/^[A-Za-z|\s]+$/g)) {
            error.name = 'Name must only contain alphabetical characters and whitespace';
        }

        if (email.trim().length === 0) {
            error.email = 'Email must not be left empty';
        } else if (email.length > 75) {
            error.email = 'Email must not exceed 75 characters';
        }

        const hasErrors = Object.keys(error).length > 0;

        if (hasErrors) {
            return res.status(422).json(error);
        }

        const params = {
            email,
            name,
        };

        updateUser(params, userId)
            .then(() => getUser(userId))
            .then((response: any) => {
                const { password, ...user } = response.user;
                res.status(response.code).json(user);
            })
            .catch((err: any) => {
                const { code, message } = err;

                if (code === 500) {
                    res.status(code).json({ error: message });
                } else {
                    res.status(code).json({ email: message });
                }
            });
    } catch (error) {
        res.sendStatus(500);
    }
});

router.put('/password', verifyUser, (req: any, res: any) => {
    const payload: ChangePassword = req.body || {};
    const payloadIsEmpty: boolean = Object.keys(payload).length === 0;

    if (payloadIsEmpty) {
        return res.status(422).json({ message: 'Request body is missing or empty' });
    }

    try {
        const userId = req.userId;
        const currentPassword: string = payload.current_password || '';
        const confirmPassword: string = payload.confirm_password || '';
        const newPassword: string = payload.new_password || '';

        const error: any = {};

        if (newPassword !== confirmPassword) {
            return res.status(401).json({ confirm_password: 'Passwords do not match' });
        }

        if (currentPassword === newPassword) {
            return res.status(409).json({ new_password: 'New password must not match current password' });
        }

        if (newPassword.length < 6) {
            error.new_password = 'Password must be at least 6 characters';
        } else if (newPassword.includes(' ')) {
            error.new_password = 'Password must not contain whitespace';
        }

        const hasErrors = Object.keys(error).length > 0;

        if (hasErrors) {
            return res.status(422).json(error);
        }

        getUser(userId)
            .then((response: any) => {
                const { user } = response;
                const { password } = user;
                const passwordIsValid = bcrypt.compareSync(currentPassword, password);

                if (!passwordIsValid) {
                    throw { code: 401, message: { current_password: 'Password was incorrect' } };
                }
            })
            .then(() => {
                updateUserPassword(newPassword, userId)
                    .then(() => res.sendStatus(204))
                    .catch((err: any) => res.status(err.code).json({ error: err.message }));
            })
            .catch((err: any) => {
                if (err.hasOwnProperty('code') && typeof err.code === 'number') {
                    return res.status(err.code).json(err.message);
                }

                res.sendStatus(500);
            });

    } catch (error) {
        res.sendStatus(500);
    }
});

router.put('/deactivate', verifyUser, (req: any, res) => {
    const userId = req.userId;
    const params = {
        role: 'deactivated',
    };

    updateUser(params, userId)
        .then(() => res.sendStatus(204))
        .catch((error: any) => res.status(error.code).json({ error: 'Unable to deactivate account' }));

});

type User = {
    name: string | '' | null;
    email: string | '' | null;
    password: string | '' | null;
}

type ChangePassword = {
    current_password: string | '' | null;
    confirm_password: string | '' | null;
    new_password: string | '' | null;
}

type responseError = {
    name?: string | '';
    email?: string | '';
    password?: string | '';
};

export default router;
