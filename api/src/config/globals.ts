import * as dev from './env/development';
import * as prod from './env/production';

const isProduction = process.env.NODE_ENV === 'production';
const env = isProduction ? prod : dev;

export const DB_HOST = env.DB_HOST;
export const DB_PASS = env.DB_PASS;
export const DB_USER = env.DB_USER;
export const DB_NAME = env.DB_NAME;
export const DB_PORT = env.DB_PORT;

export const JWT_SECRET_KEY = env.JWT_SECRET_KEY;

export const PYTHON_API_URL = env.PYTHON_API_URL;

export const MODEL_BASE_DIRECTORY = env.MODEL_BASE_DIRECTORY;
