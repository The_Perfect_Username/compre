import mysql from 'mysql';
import {
    DB_HOST,
    DB_NAME,
    DB_PASS,
    DB_PORT,
    DB_USER,
} from './globals';

const connection = mysql.createConnection({
    database : DB_NAME,
    host     : DB_HOST,
    password : DB_PASS,
    port     : DB_PORT,
    user     : DB_USER,
});

export default connection;
