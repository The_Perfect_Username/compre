import jwt from 'jsonwebtoken';
import sid from 'shortid';
import connection from '../config/db';
import { JWT_SECRET_KEY } from '../config/globals';

export const verifyUser = (req: any, res: any, next: any): void => {
    const authorization = req.headers ? (req.headers.authorization || null) : null;

    if (!authorization || authorization.indexOf('Bearer') === -1) {
        return res.status(403).send({ message: 'Bad Authorization Header' });
    }

    const authHeaderData = authorization.split(' ');

    if (authHeaderData.length !== 2) {
        return res.status(401).json({ message: 'Bad Authorization Header' });
    }

    try {
        const token = authHeaderData[1];
        const jwtDecoded: any = jwt.verify(token, JWT_SECRET_KEY);

        if (Object.keys(jwtDecoded).length === 1) {
            throw Error('No data found inside of decoded JWT');
        }

        const userId = jwtDecoded.id || null;
        const verificationToken = jwtDecoded.token || null;

        if (!userId || !sid.isValid(userId)) {
            throw Error('User ID is invalid');
        }

        if (!verificationToken) {
            throw Error('Verification token is missing');
        }

        connection.query('SELECT 1, role FROM users WHERE id = ? AND token = ?',
            [userId, verificationToken],
            (error, results) => {

            if (error) {
                console.error(error);
                return res.status(500).json({ message: 'An unxpected database error occurred' });
            }

            if (results.length === 0) {
                return res.status(401).json({ message: 'Unable to authorise user' });
            }

            const role = results[0].role;

            if (role === 'banned') {
                return res.status(403).json({ message: 'Account is banned for misuse', is_banned: true, });
            }

            req.userId = userId;
            req.isAdmin = role === 'admin';

            next();

        });

    } catch (error) {
        console.error(error.message);
        return res.status(401).json({ message: 'Unable to verify token' });
    }
};

export const adminOnly = (req: any, res: any, next: any) => {
    if (!req.isAdmin) {
        return res.status(403).json({ error: 'Unauthorised access' });
    }

    next();
};
