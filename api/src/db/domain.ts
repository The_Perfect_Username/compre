import connection from '../config/db';

export const createDomain = (name: string) => {
    return new Promise((resolve, reject) => {
        const id = name.toLowerCase().replace(/\s/g, '_');
        const created = new Date().toISOString().replace('T', ' ').substr(0, 19);
        const params = {
            created,
            id,
            name,
        };

        connection.query('INSERT INTO domains SET ?', params, (error) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Unable to create domain',
                });
            }

            resolve({ id });
        });
    });
};

export const updateDomainName = (name: string, domainId: string) => {
    return new Promise((resolve, reject) => {
        const id = name.toLowerCase().replace(/\s/g, '_');
        const params = {
            id,
            name,
        };

        connection.query('UPDATE domains SET ? WHERE id = ?', [params, domainId], (error) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Unable to create domain',
                });
            }

            resolve({ id });
        });
    });
};

export const updateDomainStatus = (status: string, domainId: string) => {
    return new Promise((resolve, reject) => {
        const params = {
            status,
        };

        connection.query('UPDATE domains SET ? WHERE id = ?', [params, domainId], (error) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Unable to create domain',
                });
            }

            resolve(true);
        });
    });
};

export const getDomain = (id: string) => {
    return new Promise((resolve, reject) => {
        const query = 'SELECT id, name, status, created FROM domains WHERE id = ?';
        connection.query(query, [id], (error, results) => {
            if (error) {
                return reject({ code: 500, message: 'Unable to retrieve domain' });
            }

            const result = results.length > 0 ? results[0] : {};

            resolve({
                code: 200,
                results: result,
            });
        });
    });
};

export const fetchDomains = (limit: number, offset: number) => {
    return new Promise((resolve, reject) => {
        const query = 'SELECT id, name, status, created FROM domains ORDER BY UNIX_TIMESTAMP(created) DESC LIMIT ? OFFSET ?';
        connection.query(query, [limit, offset], (error, results) => {
            if (error) {
                return reject({ code: 500, message: 'Unable to retrieve domains' });
            }

            resolve({
                code: 200,
                results,
            });
        });
    });
};

export const fetchDomainsByTerms = (searchTerms: string, limit: number, offset: number) => {
    return new Promise((resolve, reject) => {
        const terms = `${searchTerms.trim().replace(/\s/g, '* ')}*`;
        const params = [terms, terms, limit, offset];
        const query = `SELECT
            MATCH(D.name) AGAINST(? IN BOOLEAN MODE) AS relevance,
            D.id AS id,
            D.name AS name,
            D.status AS status,
            D.created AS created
            FROM domains AS D
            WHERE MATCH(D.name) AGAINST(? IN BOOLEAN MODE) ORDER BY relevance DESC LIMIT ? OFFSET ?`;

        connection.query(query, params, (error, results) => {
            if (error) {
                return reject({ code: 500, message: 'Unable to retrieve domains' });
            }

            resolve({
                code: 200,
                results,
            });
        });
    });
};
