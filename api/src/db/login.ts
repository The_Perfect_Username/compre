import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import connection from '../config/db';
import { JWT_SECRET_KEY } from '../config/globals';

export const login = (email: string, plainTextPassword: string) => {
    return new Promise((resolve, reject) => {
        const query = 'SELECT id, name, password, token, role FROM users WHERE email = ?';

        connection.query(query, [email], (error, results) => {
            if (error) {
                console.error(error);
                return reject({
                    code: 500,
                    message: 'An unexpected error occurred',
                });
            }

            const userExists = results.length === 1;

            if (!userExists) {
                return reject({
                    code: 401,
                    message: 'Email/password combination is incorrect',
                });
            }

            const [user] = results;

            const {
                id,
                name,
                password,
                token,
                role,
            } = user;

            const passwordIsValid = bcrypt.compareSync(plainTextPassword, password);

            if (!passwordIsValid) {
                return reject({
                    code: 401,
                    message: 'Email/password combination is incorrect',
                });
            }

            const jwtToken = jwt.sign({
                id,
                token,
            }, JWT_SECRET_KEY);

            const isAdmin = role === 'admin';

            const userData = {
                email,
                id,
                is_admin: isAdmin,
                name,
                token: jwtToken,
            };

            if (!isAdmin) {
                delete userData.is_admin;
            }

            resolve({
                code: 200,
                user: userData,
            });
        });
    });
};
