import sid from 'shortid';
import connection from '../config/db';
import { createdChallengeObject } from '../lib/challenges';

export const createChallenge = (challenge: string, userId: string, domainId: string) => {
    return new Promise((resolve, reject) => {
        const id = sid.generate();
        const created = new Date().toISOString().replace('T', ' ').substr(0, 19);
        const params = {
            challenge,
            created,
            domain_id: domainId,
            id,
            user_id: userId,
        };

        const query = 'INSERT INTO challenges SET ?';

        connection.query(query, [params], (error) => {
            if (error) {
                return reject({ code: 500, message: 'Unable to create challenge' });
            }

            resolve({
                code: 200,
                id,
            });
        });
    });
};

export const getChallenge = (id: string) => {
    return new Promise((resolve, reject) => {
        const query = `
        WITH domainGroup AS
            (SELECT id, domain_id, challenge, status, created, row_number() OVER (ORDER BY created) AS rowNumber FROM challenges
            WHERE domain_id = (SELECT domain_id FROM compre_dev.challenges WHERE id = ?) AND status = 'published')
        SELECT
            id,
            (SELECT name from domains WHERE id = domainGroup.domain_id) AS domain,
            domain_id,
            challenge,
            status,
            created
        FROM
            domainGroup
        WHERE
            rowNumber
        BETWEEN
            (SELECT rowNumber FROM domainGroup WHERE id = ?) - 1
            AND (SELECT rowNumber FROM domainGroup WHERE id = ?) + 1;
        `;

        connection.query(query, [id, id, id], (error, results) => {
            if (error) {
                console.error(error);
                return reject({ code: 500, message: 'Unable to retrieve challenge' });
            }

            const result = results.length > 0 ? createdChallengeObject(id, results) : {};

            resolve({ code: 200, results: result });
        });
    });
};

export const fetchChallenges = (domainId: string, limit: number, offset: number) => {
    return new Promise((resolve, reject) => {
        const query = `
            SELECT
                id, challenge, status, created
            FROM
                challenges
            WHERE
                domain_id = ?
            ORDER BY
                UNIX_TIMESTAMP(created) ASC LIMIT ? OFFSET ?`;
        connection.query(query, [domainId, limit, offset], (error, results) => {
            if (error) {
                return reject({ code: 500, message: 'Unable to retrieve challenge' });
            }

            resolve({ code: 200, results });
        });
    });
};

export const fetchChallengesBySearch = (searchTerms: string, userId: string, limit: number, offset: number) => {
    return new Promise((resolve, reject) => {
        const terms = `${searchTerms.trim().replace(/\s/g, '* ')}*`;
        const params = [terms, userId, userId, terms, limit, offset];
        const query = `
            SELECT
                MATCH(C.challenge) AGAINST(? IN BOOLEAN MODE) AS relevance, C.id AS id, C.challenge AS challenge,
                (SELECT answer FROM user_answers WHERE user_id = ? AND challenge_id = C.id ORDER BY UNIX_TIMESTAMP(created) DESC LIMIT 1) AS answer,
                (SELECT status FROM user_answers WHERE user_id = ? AND challenge_id = C.id ORDER BY UNIX_TIMESTAMP(created) DESC LIMIT 1) AS status
            FROM
                challenges AS C
            WHERE
                MATCH(C.challenge) AGAINST(? IN BOOLEAN MODE) ORDER BY relevance ASC LIMIT ? OFFSET ?`;
        connection.query(query, params, (error, results) => {
            if (error) {
                console.error(error);
                return reject({ code: 500, message: 'Unable to fetch challenges' });
            }

            resolve({ code: 200, results });
        });
    });
};

export const updateChallenge = (params: any, challengeId: string) => {
    return new Promise((resolve, reject) => {
        connection.query('UPDATE challenges SET ? WHERE id = ?', [params, challengeId], (error) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Unable to update challenge',
                });
            }

            resolve(true);
        });
    });
};

export const publishChallenges = (domainId: string) => {
    return new Promise((resolve, reject) => {
        connection.query('UPDATE challenges SET status = ? WHERE domain_id = ?', ['published', domainId], (error) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Unable to update challenge status',
                });
            }

            resolve(true);
        });
    });
};

export const fetchChallengesForUser = (domainId: string, userId: string, limit: number, offset: number) => {
    return new Promise((resolve, reject) => {
        const query = `
            SELECT
                C.id AS id, C.challenge AS challenge,
                (
                    SELECT
                        answer
                    FROM
                        user_answers
                    WHERE
                        user_id = ? AND challenge_id = C.id
                        ORDER BY UNIX_TIMESTAMP(created) DESC LIMIT 1
                ) AS answer,
                (
                    SELECT
                        status
                    FROM
                        user_answers
                    WHERE
                        user_id = ? AND challenge_id = C.id
                        ORDER BY UNIX_TIMESTAMP(created) DESC LIMIT 1
                ) AS status
            FROM
                challenges AS C
            WHERE
                C.domain_id = ? AND C.status = ?
                ORDER BY UNIX_TIMESTAMP(created) ASC LIMIT ? OFFSET ?`;
        const params = [userId, userId, domainId, 'published', limit, offset];

        connection.query(query, params, (error, results) => {
            if (error) {
                console.error(error);
                return reject({
                    code: 500,
                    message: 'Was unable to retrieve challenges',
                });
            }

            resolve({
                code: 200,
                results,
            });
        });
    });
};

export const fetchDomainChallenges = (domainId: string, limit: number, offset: number) => {
    return new Promise((resolve, reject) => {
        const query = `
            SELECT
                id, challenge, status, created
            FROM
                challenges
            WHERE
                domain_id = ?
            ORDER BY UNIX_TIMESTAMP(created) ASC LIMIT ? OFFSET ?`;

        connection.query(query, [domainId, limit, offset], (error, results) => {
            if (error) {
                return reject({ code: 500, message: 'Unable to retrieve domains' });
            }

            resolve({
                code: 200,
                results,
            });
        });
    });
};

export const fetchDomainChallengesByTerms = (domainId: string, searchTerms: string, limit: number, offset: number) => {
    return new Promise((resolve, reject) => {
        const terms = `${searchTerms.trim().replace(/\s/g, '* ')}*`;
        const params = [terms, domainId, terms, limit, offset];
        const query = `
            SELECT
                MATCH(C.challenge) AGAINST(? IN BOOLEAN MODE) AS relevance,
                C.id AS id,
                C.challenge AS challenge,
                C.status AS status,
                C.created AS created
            FROM
                challenges AS C
            WHERE
                C.domain_id = ? AND MATCH(C.challenge) AGAINST(? IN BOOLEAN MODE)
            ORDER BY relevance ASC LIMIT ? OFFSET ?`;

        connection.query(query, params, (error, results) => {
            if (error) {
                return reject({ code: 500, message: 'Unable to retrieve challenges' });
            }

            resolve({
                code: 200,
                results,
            });
        });
    });
};

export const createContestion = (challengeId: string, userId: string, answer: string) => {
    return new Promise((resolve, reject) => {
        const id = sid.generate();
        const created = new Date().toISOString().replace('T', ' ').substr(0, 19);
        const params = {
            answer,
            challenge_id: challengeId,
            created,
            id,
            user_id: userId,
        };

        const query = 'INSERT INTO contested_answers SET ?';

        connection.query(query, params, (error) => {
            if (error) {
                console.error(error);
                return reject(true);
            }

            resolve(true);
        });
    });
};
