import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import sid from 'shortid';
import uuidv4 from 'uuid/v4';
import connection from '../config/db';
import { JWT_SECRET_KEY } from '../config/globals';

export const createUser = (name: string, email: string, password: string) => {
    const id = sid.generate();
    const token = uuidv4();
    const saltRounds = 10;
    const hashedPassword = bcrypt.hashSync(password, saltRounds);
    const created = new Date().toISOString().replace('T', ' ').substr(0, 19);

    const data = {
        created,
        email,
        id,
        name,
        password: hashedPassword,
        token,
    };

    return new Promise((resolve, reject) => {
        connection.query('INSERT INTO users SET ?', [data], (error: any) => {
            if (error && error.code === 'ER_DUP_ENTRY') {
                return reject({
                    code: 409,
                    message: 'Email address already exists',
                });
            } else if (error) {
                return reject({
                    code: 409,
                    message: 'Unable to create user account',
                });
            }

            const jwtToken = jwt.sign({
                id,
                token,
            }, JWT_SECRET_KEY);

            resolve({
                code: 200,
                user: {
                    email,
                    id,
                    name,
                    token: jwtToken,
                },
            });
        });
    });
};

export const getUser = (userId: string) => {
    return new Promise((resolve, reject) => {
        connection.query('SELECT name, email, password, role FROM users WHERE id = ?', [userId], (err, results) => {
            if (err) {
                console.error(err);
                return reject({ code: 500 });
            }

            if (results.length === 0) {
                return reject({ code: 404, message: 'User not found' });
            }

            const [result] = results;
            const { name, email, password, role } = result;
            const isAdmin = role === 'admin';
            const user = {
                email,
                id: userId,
                is_admin: isAdmin,
                name,
                password,
            };

            if (!isAdmin) {
                delete user.is_admin;
            }

            resolve({
                code: 200,
                user,
            });
        });
    });
};

export const updateUser = (params: any, userId: string) => {
    return new Promise((resolve, reject) => {
        connection.query('UPDATE users SET ? WHERE id = ?', [params, userId], (error) => {
            if (error) {
                const response = error.code === 'ER_DUP_ENTRY'
                    ? {
                        code: 409,
                        message: 'Email already exists',
                    }
                    : {
                        code: 500,
                        message: 'Unable to update basic information',
                    };

                return reject(response);
            }

            resolve(true);
        });
    });
};

export const updateUserPassword = (password: string, userId: string) => {
    const saltRounds = 10;
    const hashedPassword = bcrypt.hashSync(password, saltRounds);

    return new Promise((resolve, reject) => {
        connection.query('UPDATE users SET password = ? WHERE id = ?', [hashedPassword, userId], (error) => {
            if (error) {
                console.error(error);
                return reject({
                    code: 500,
                    message: 'Was unable to update password',
                });
            }

            resolve(true);
        });
    });
};
