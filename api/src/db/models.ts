import uuidv4 from 'uuid/v4';
import connection from '../config/db';
import { MODEL_BASE_DIRECTORY } from '../config/globals';

export const moveModelFile = (file: any, id: string) => {
    return new Promise((resolve, reject) => {
        file.mv(`${MODEL_BASE_DIRECTORY}/${id}.joblib`, (error: any) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Was unable to successfully move model',
                });
            }

            resolve({ code: 200, id });
        });
    });
};

export const createModel = (challengeId: string) => {
    const modelId = uuidv4();
    const params = {
        challenge_id: challengeId,
        id: modelId,
    };

    return new Promise((resolve, reject) => {
        connection.query('INSERT INTO models SET ?', params, (error) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Unable to create model record',
                });
            }

            resolve({ id: modelId });
        });
    });
};

export const getModelByChallengeId = (challengeId: string) => {
    return new Promise((resolve, reject) => {
        const query = 'SELECT id FROM models WHERE challenge_id = ?';
        connection.query(query, [challengeId], (error, results) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Unable to create model record',
                });
            }

            if (results.length === 0) {
                return resolve({});
            }

            resolve(results[0]);
        });
    });
};

export const deleteModelByChallengeId = (challengeId: string) => {
    return new Promise((resolve, reject) => {
        const query = 'DELETE FROM models WHERE challenge_id = ?';
        connection.query(query, [challengeId], (error) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Unable to delete model record',
                });
            }

            resolve(true);
        });
    });
};
