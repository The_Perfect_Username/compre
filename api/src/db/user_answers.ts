import sid from 'shortid';
import connection from '../config/db';

export const calculateStatus = (score: number) => {
    if (score < 0.5) {
        return 'weak';
    } else if (score >= 0.5 && score < 0.75) {
        return 'good';
    } else {
        return 'strong';
    }
};

export const getAnswer = (userId: string, challengeId: string) => {
    return new Promise((resolve, reject) => {
        const query = `
        SELECT
            id, answer, status
        FROM
            user_answers
        WHERE
            user_id = ? AND challenge_id = ?
            ORDER BY UNIX_TIMESTAMP(created) DESC LIMIT 1`;
        connection.query(query, [userId, challengeId], (error, results) => {
            if (error) {
                return reject({
                    code: 500,
                    message: 'Was unable to fetch answer',
                });
            }

            const result = results.length > 0 ? results[0] : {};

            resolve({
                code: 200,
                results: result,
            });
        });
    });
};

export const createAnswer = (userId: string, challengeId: string, answer: string, score: number, status: string) => {
    return new Promise((resolve, reject) => {

        const id = sid.generate();
        const created = new Date().toISOString().replace('T', ' ').substr(0, 19);

        const params = {
            answer,
            challenge_id: challengeId,
            created,
            id,
            score,
            status,
            user_id: userId,
        };

        const query = 'INSERT INTO user_answers SET ?';

        connection.query(query, [params], (error) => {
            if (error) {
                console.error(error);
                return reject({ code: 500, message: 'Was unable to create answer'});
            }

            resolve(true);
        });
    });
};
