import bodyParser from 'body-parser';
import express from 'express';
import fileUpload from 'express-fileupload';
import Answer from './routes/answer';
import Challenge from './routes/challenge';
import Domain from './routes/domain';
import Login from './routes/login';
import Search from './routes/search';
import User from './routes/user';

const app = express();
const port = 3001;

app.use(fileUpload({
    limits: { fileSize: 3 * 1024 * 1024 },
    tempFileDir: '/tmp/',
    useTempFiles: true,
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use((_, res, next) => {
    res.header('Access-Control-Allow-Origin', 'http://localhost:3000');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', 'DELETE, POST, PUT, GET, OPTIONS');
    next();
});

app.use('/answer', Answer);
app.use('/domain', Domain);
app.use('/login', Login);
app.use('/challenge', Challenge);
app.use('/search', Search);
app.use('/user', User);

app.get('/', (_, res) => {
    res.send('Hello World!');
});

app.listen(port);
