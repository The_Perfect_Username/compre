#!/usr/bin/python
import sys, getopt, json
import csv
from joblib import dump
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

def train_corpus(corpus, file_name):
    print ("Beginning training")

    tf_idf_vectorizer = TfidfVectorizer(analyzer='word', ngram_range=(1,2), min_df=0, stop_words='english')

    tf_idf_matrix = tf_idf_vectorizer.fit_transform(corpus)

    tf_maxtrix_and_model = (tf_idf_vectorizer, tf_idf_matrix)

    dump(tf_maxtrix_and_model, 'models/{}.joblib'.format(file_name))

    print ("Training complete")

def read_csv(target_file):
    with open(target_file, 'r') as f:
        reader = csv.reader(f)
        return list(reader)

def main(argv):
    try:
        # Script parameters
        opts, _ = getopt.getopt(argv, "hf:", ["source=", "dest="])

        # Source file and desination file name
        source_file = ''
        desitination_file = ''

        # Retrieve the parameter values
        for opt, arg in opts:
            if opt in ("--source"):
                source_file = arg

            if opt in ("--dest"):
                desitination_file = arg

        if len(source_file) == 0:
            print('File directory is missing')
            sys.exit(2)

        if len(desitination_file) == 0:
            print('File name is missing')
            sys.exit(2)

        # Flatten CSV list into a 1D list
        corpus = [line[0] for line in read_csv(source_file)]

        # Create trained model
        train_corpus(corpus, desitination_file)

    except getopt.GetoptError as E:
        print(E)
        sys.exit(2)

if __name__ == "__main__":
    main(sys.argv[1:])


'''
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel

tf = TfidfVectorizer(analyzer='word', ngram_range=(1,3), min_df = 0, stop_words = 'english')
tfidf_matrix = tf.fit_transform(corpus)



tf2 = TfidfVectorizer(analyzer='word', ngram_range=(1,3), min_df = 0, stop_words = 'english', vocabulary=tf.vocabulary_)
tfidf_matrix_2 = tf2.fit_transform(["I would enjoy a piece of database administration"])

for index, score in find_similar(tfidf_matrix, tfidf_matrix_2):
    print (score, corpus[index])
'''