#!/usr/bin/python
import os
import sys, getopt, json
from joblib import load
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import linear_kernel
from config.globals import MODEL_BASE_DIRECTORY

def score(answer, model_id):
    print (MODEL_BASE_DIRECTORY)
    tf, tfidf_matrix = load('{}/{}.joblib'.format(MODEL_BASE_DIRECTORY, model_id))
    tf2 = TfidfVectorizer(analyzer='word', ngram_range=(1,2), min_df = 0, stop_words = 'english', vocabulary=tf.vocabulary_)
    tfidf_matrix_2 = tf2.fit_transform([answer])

    top_score = find_similar(tfidf_matrix, tfidf_matrix_2)[0]
    print (top_score)
    return top_score

def find_similar(tfidf_matrix, target, top_n = 1):
    cosine_similarities = linear_kernel(target, tfidf_matrix).flatten()
    related_docs_indices = [i for i in cosine_similarities.argsort()[::-1]]

    return [cosine_similarities[index] for index in related_docs_indices][0:top_n]
