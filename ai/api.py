from flask import Flask
from flask_restful import reqparse, Resource, Api

from lib.score import score

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('answer', type=str, help='User answer to the provided challenge')

class ScoreUserInput(Resource):
    def put(self, model_id):
        args = parser.parse_args(strict=True)

        answer = args['answer']

        similarity_score = score(answer, model_id)

        return similarity_score

api.add_resource(ScoreUserInput, '/score/<model_id>')

if __name__ == '__main__':
    app.run(debug=True)