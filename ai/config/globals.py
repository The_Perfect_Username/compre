import os

import config.env.development as development
import config.env.production as production

is_production = os.environ.get('NODE_ENV') == 'production'

env = production if is_production else development

MODEL_BASE_DIRECTORY = env.MODEL_BASE_DIRECTORY
