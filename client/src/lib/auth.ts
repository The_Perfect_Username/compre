export const userIsLoggedIn = () => {
    const localStorage = window.localStorage;
    const token = localStorage.getItem('token');
    return !!token;
}

export const isAdmin = () => {
    const localStorage = window.localStorage;
    const user = localStorage.getItem('user');

    if (user) {
        const userData = JSON.parse(user);
        const { is_admin } = userData;
        return !!is_admin;
    }

    return false;
}

export const getAccount = () => {
    const userDetails = window.localStorage.getItem('user');

    if (!userDetails) {
        throw Error('No account details were found.');
    }

    const userData = JSON.parse(userDetails);

    return userData;
}
