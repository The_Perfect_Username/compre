export const handleError = (response: any) => {
    if (!response) {
        return { error: 'An unexpected error occurred' };
    } else if (response.hasOwnProperty('data')) {
        return response.data;
    }

    return {};
}