import * as dev from './env/development';
import * as prod from './env/production';

const isProduction = process.env.NODE_ENV === 'production';
const env = isProduction ? prod : dev;

export const API_URL = env.API_URL;
export const CLIENT_URL = env.CLIENT_URL;