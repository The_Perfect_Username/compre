const protocol = window.location.protocol;

export const API_URL = `${protocol}//api.kassess.com`;
export const CLIENT_URL = `${protocol}//www.kassess.com`;