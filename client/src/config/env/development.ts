const protocol = window.location.protocol;

export const API_URL = `${protocol}//localhost:3001`;
export const CLIENT_URL = `${protocol}//localhost:3000`;