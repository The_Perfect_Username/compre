import React from 'react';
import { Link } from 'react-router-dom';
import BeatLoader from 'react-spinners/BeatLoader';
import Status, { StatusLabels } from '../challenge/status';
import SearchBar from '../search/searchbar';

const Table = (props: any): JSX.Element => {
    const {
        results,
        loading,
        onQueryChange,
        fetchResults,
    } = props;

    const noResults = results.length === 0 && !loading;

    return (
        <div>
            <div className="table-header d-flex reverse">
                <SearchBar
                    placeholder="Search..."
                    onChange={onQueryChange}
                    onSubmit={fetchResults}
                />
            </div>
            <div className="table-body">
                {results}
                {loading && <TableLoader />}
                <NoResultsRow visible={noResults} />
            </div>
        </div>
    );
};

export const TableRow = (props: IRowProps) => {
    const {
        status,
        text,
        goto,
    } = props;

    if (status) {
        return (
            <div className="row">
                <Link className="d-flex spaced" to={goto}>
                    <div className="d-65 cell">
                        <span className="label capitalise">{text}</span>
                    </div>
                    <div className="d-35 cell d-flex reverse center">
                        <Status
                            defaultLabel="training"
                            label={status}
                            status={status}
                        />
                    </div>
                </Link>
            </div>
        );
    }

    return (
        <div className="row">
            <Link className="d-flex spaced" to={goto}>
                <div className="cell">
                    <span className="label capitalise">{text}</span>
                </div>
            </Link>
        </div>
    );
};

export const NoResultsRow = (props: any) => {
    const { visible } = props;

    if (!visible) {
        return <></>;
    }

    return (
        <div className="row no-results">
            <span>No Results</span>
        </div>
    );
}

export const TableLoader = () => {
    const override = `
            padding: 1px;
            display: flex;
            justify-content: center;
            align-items: center;
        `;

    return (
        <div className="loader">
            <BeatLoader css={override} />
        </div>
    );
};

interface IRowProps {
    goto: string;
    status?: StatusLabels;
    text: string;
};

export default Table;