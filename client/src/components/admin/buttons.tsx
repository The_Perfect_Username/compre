import React, { ReactChild } from 'react';
import { Link } from 'react-router-dom';

const ActionButton = (props: ActionButtonProps) => {
    const { children, to } = props;

    return (
        <Link className="btn btn-default mr-1" to={to} >
            {children}
        </Link>
    );
};

type ActionButtonProps = {
    children: ReactChild;
    to: string;
}

export default ActionButton;