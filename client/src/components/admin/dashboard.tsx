import axios from 'axios';
import React, { Component, FormEvent, ChangeEvent } from 'react';
import { withRouter } from 'react-router';
import ActionButton from './buttons';
import Table, { TableRow } from './table';
import BreadCrumbs from '../layout/breadcrumbs';
import { API_URL } from '../../config/globals';

class Dashboard extends Component<any> {
    render() {
        return (
            <div className="container pt-40">
                <div className="content">
                    <BreadCrumbs crumbs={['Domains']} />
                    <div className="mb-30">
                        <DomainTable />
                    </div>
                    <div>
                        <ActionButton to='/admin/dashboard/domain/create'>
                            Create Domain
                        </ActionButton>
                        <ActionButton to='/admin/dashboard/challenge/create'>
                            Create Challenge
                        </ActionButton>
                    </div>
                </div>
            </div>
        );
    };
};

class DomainTable extends Component {
    state = {
        query: '',
        results: [],
        loading: true,
    }

    fetchResults = (e: FormEvent) => {
        e.preventDefault();

        const { query } = this.state;
        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        axios.get(`${API_URL}/domain/fetch?search_query=${query}`, { headers })
            .then(response => {
                const { data } = response;
                this.setState({ results: data, loading: false });
            })
            .catch(error => console.error(error));
    }

    setQuery = (e: ChangeEvent) => {
        const target: any = e.currentTarget;

        if (target) {
            const query: string = target.value;
            this.setState({ query });
        }
    }

    componentDidMount() {
        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        axios.get(`${API_URL}/domain/fetch`, { headers })
            .then(response => {
                const { data } = response;
                this.setState({ results: data, loading: false });
            })
            .catch(error => console.error(error));
    }

    render() {
        const { results, loading } = this.state;
        const rows = results.map((result: any, index: number) => {
            const { id, name, status } = result;
            return (
                <TableRow
                    key={index}
                    goto={`/admin/dashboard/${id}`}
                    text={name}
                    status={status}
                />
            );
        });

        return (
            <Table
                results={rows}
                loading={loading}
                onQueryChange={this.setQuery}
                fetchResults={this.fetchResults}
            />
        );
    };
};

export default withRouter(Dashboard);