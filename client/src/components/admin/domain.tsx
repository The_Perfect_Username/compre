import axios from 'axios';
import React, { FormEvent, ChangeEvent, useState, useEffect, useCallback } from 'react';
import { withRouter } from 'react-router-dom';
import ActionButton from './buttons';
import Table, { TableRow } from './table';
import { FormGroup, LabelWithCounter, Button, ErrorMessage } from '../form/form';
import BreadCrumbs from '../layout/breadcrumbs';
import { API_URL } from '../../config/globals';
import { handleError } from '../../lib/error';

const DomainDashboard = (props: any): JSX.Element => {
    const [name, setName] = useState<string>('');
    const [status, setStatus] = useState<string>('');

    const domainId = props.match.params.id;

    const fetchDomainData = useCallback(() => {
        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        axios.get(`${API_URL}/domain/${domainId}`, { headers })
            .then(response => {
                const { data } = response;
                const { name, status } = data;

                setName(name);
                setStatus(status);
            })
            .catch(error => console.error(error));
    }, [domainId]);

    const updateName = (name: string): void => setName(name);

    useEffect(() => {
        fetchDomainData();
    }, [fetchDomainData]);

    return (
        <div className="container pt-40">
            <div className="content">
                <BreadCrumbs crumbs={['Domains', name]} />
                <UpdateDomainName
                    id={domainId}
                    name={name}
                    updateName={updateName}
                    {...props}
                />
                <DomainTable {...props} />
                <DashboardActionButtons
                    id={domainId}
                    status={status}
                />
            </div>
        </div>
    );
};

const DomainTable = (props: any): JSX.Element => {
    const [query, setQuery] = useState<string>('');
    const [results, setResults] = useState<Result[]>([]);
    const [loading, setLoading] = useState<boolean>(true);

    const domainId = props.match.params.id;

    const fetchChallenges = useCallback(() => {
        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        axios.get(`${API_URL}/challenge/fetch/${domainId}`, { headers })
            .then(response => {
                const { data } = response;
                setResults(data);
                setLoading(false);
            })
            .catch(error => console.error(error));
    }, [domainId]);

    const fetchResults = (e: FormEvent) => {
        e.preventDefault();
        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        axios.get(`${API_URL}/challenge/fetch/${domainId}?search_query=${query}`, { headers })
            .then(response => {
                const { data } = response;
                setResults(data);
                setLoading(false);
            })
            .catch(error => console.error(error));
    }

    const onQueryChange = (e: ChangeEvent) => {
        const target: any = e.currentTarget;

        if (target) {
            const query: string = target.value;
            setQuery(query);
        }
    }

    const rows = results.map((result: any, index: number) => {
        const { id, challenge } = result;

        return (
            <TableRow
                key={index}
                goto={`/admin/dashboard/challenge/${id}`}
                text={challenge}
            />
        );
    });

    useEffect(() => {
        fetchChallenges();
    }, [domainId, fetchChallenges]);

    return (
        <FormGroup>
            <Table
                results={rows}
                loading={loading}
                onQueryChange={onQueryChange}
                fetchResults={fetchResults}
            />
        </FormGroup>
    );

};

const DashboardActionButtons = (props: any): JSX.Element => {
    const [status, setStatus] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>('');

    const { status: currentStatus, id: domainId } = props;

    useEffect(() => {
        setStatus(currentStatus);
    }, [currentStatus]);

    const onClick = (e: Event) => {
        e.preventDefault();

        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        setLoading(true);

        axios.put(`${API_URL}/domain/publish/${domainId}`, null, { headers })
            .then(() => {
                setLoading(false);
                setStatus('published');
            })
            .catch((error) => {
                const { response } = error;
                const errorResponse = handleError(response);
                setError(errorResponse.error);
                setLoading(false);
            });
    }

    const published = status === 'published';

    let buttonLabel = 'Publish';

    if (published) {
        buttonLabel = 'Published';
    } else if (status === 'modified') {
        buttonLabel = 'Republish';
    }

    return (
        <div>
            <FormGroup>
                <label>Actions</label>
                <div className="d-flex">
                    <Button
                        className="btn btn-default mr-1"
                        loading={loading}
                        onClick={onClick}
                        disabled={published}
                    >
                        {buttonLabel}
                    </Button>
                    <ActionButton to={`/admin/dashboard/challenge/create?domain=${domainId}`}>
                        Create Challenge
                    </ActionButton>
                </div>
            </FormGroup>
            <ErrorMessage message={error} />
        </div>
    );
};

const UpdateDomainName = (props: any): JSX.Element => {
    const [name, setName] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [error, setError] = useState<string>('');

    const { id: domainId, name: domainName, updateName } = props;

    useEffect(() => {
        setName(domainName);
    }, [domainName]);

    const onSubmit = (e: FormEvent) => {
        e.preventDefault();
        const data = { name };
        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        setLoading(true);

        axios.put(`${API_URL}/domain/${domainId}`, data , { headers })
            .then(response => {
                const { data } = response;
                const { id } = data;

                updateName(name);
                setLoading(false);
                setError('');

                props.history.push(`/admin/dashboard/${id}`);
            })
            .catch(error => {
                const { response } = error;
                const errorResponse = handleError(response);
                setError(errorResponse.error);
                setLoading(false);
            });
    }

    const onChange = (e: ChangeEvent) => {
        const { value }: any = e.currentTarget;
        setName(value);
    }

    return (
        <form onSubmit={onSubmit}>
            <FormGroup>
                <div className="d-flex">
                    <div className="d-width-100">
                        <LabelWithCounter value={name} maxLength={100}>
                            Domain Name
                        </LabelWithCounter>
                        <input
                            className="form-control"
                            onChange={onChange}
                            type="text"
                            maxLength={100}
                            value={name}
                        />
                    </div>
                    <div className="inline-button-container">
                        <Button
                            className="btn btn-default"
                            loading={loading}
                        >
                            Update
                        </Button>
                    </div>
                </div>
                <ErrorMessage message={error} />
            </FormGroup>
        </form>
    );
};

type Result = {
    id: string;
    challenge: string;
}

export default withRouter(DomainDashboard);