import axios from 'axios';
import React, { useCallback, useEffect, useState } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { FormGroup, ErrorMessage, Button } from '../form/form';
import { API_URL } from '../../config/globals';
import { Tile } from '../layout/tiles';
import { handleError } from '../../lib/error';

type Errors = {
    file: string;
    error: string;
}

const DashboardChallenge = (props: any): JSX.Element => {
    const [domain, setDomain] = useState<string>('');
    const [challenge, setChallenge] = useState<string>('');
    const [file, setFile] = useState<File | ''>('');
    const [fetchLoading, setFetchLoading] = useState<boolean>(false);
    const [updateLoading, setUpdateLoading] = useState<boolean>(false);
    const [errors, setErrors] = useState<Errors>({
        file: '',
        error: '',
    });

    const challengeId = props.match.params.id;

    const initialFetch = useCallback(() => {
        setFetchLoading(true);
        axios.get(`${API_URL}/challenge?id=${challengeId}`)
            .then(response => {
                const { data } = response;

                setDomain(data.domain);
                setChallenge(data.challenge);
            })
            .catch(error => console.error(error))
            .finally(() => setFetchLoading(false));
    }, [challengeId]);

    useEffect(() => {
        initialFetch();
    }, [initialFetch]);

    const onClick = (e: any) => {
        e.preventDefault();
        const fileUploadElement: HTMLElement | null = document.getElementById('fileUpload');

        if (fileUploadElement) {
            fileUploadElement.click();
        }
    }

    const onFileChange = (e: any) => {
        const input = e.currentTarget;
        const file: File = input.files[0];

        setFile(file);
    }

    const onSubmit = (e: any) => {
        e.preventDefault();

        const data = new FormData();

        setUpdateLoading(true);

        data.set('domain', domain);
        data.set('challenge', challenge);
        data.append('file', file);

        axios({
            headers: {
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
            url: `${API_URL}/challenge/${challengeId}/model`,
            method: 'put',
            data,
        }).then(() => {
            setErrors({ file: '', error: '' });
            setUpdateLoading(false);
        })
        .catch(error => {
            const { response } = error;
            const errorResponse = handleError(response);
            setErrors({ ...errorResponse });
            setUpdateLoading(false);
        });
    }

    const fileInputLabel =  (!!file && file.name) || 'No file selected';

    return (
        <div className="container pt-40 d-flex spaced">
            <div className="content">
                <form onSubmit={onSubmit}>
                    <FormGroup>
                        <label>Domain</label>
                        <Tile loading={fetchLoading} type="title">
                            <h4 className="h-light margin-0">{domain}</h4>
                        </Tile>
                    </FormGroup>
                    <FormGroup>
                        <label>Challenge</label>
                        <Tile loading={fetchLoading} type="title">
                            <Link
                                to={`/challenge/${challengeId}`}
                                className="h-light no-decoration d-inline-block"
                            >
                                {challenge}
                            </Link>
                        </Tile>
                    </FormGroup>
                    <FormGroup>
                        <label>Update Challenge Model</label>
                        <div className="file-upload">
                            <input type="file" id="fileUpload" onChange={onFileChange} />
                            <button onClick={onClick} className="btn">Upload Model</button>
                            <span>{fileInputLabel}</span>
                        </div>
                        <ErrorMessage message={errors.file} />
                    </FormGroup>
                    <FormGroup>
                        <div className="d-flex reverse space-buttons">
                            <Button
                                className="btn btn-default"
                                loading={updateLoading}
                            >
                                Submit
                            </Button>
                        </div>
                    </FormGroup>
                    <ErrorMessage message={errors.error} />
                </form>
            </div>
        </div>
    );
};

export default withRouter(DashboardChallenge);
