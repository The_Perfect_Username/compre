import React from 'react';
import { Switch, Route } from 'react-router-dom';
import DashboardChallenge from '../admin/challenge';
import UserDashboard from '../dashboard/dashboard';
import AdminDashboard from '../admin/dashboard';
import DomainDashboard from '../admin/domain';
import CreateDomain from '../domains/create';
import Domain from '../domains/domain';
import LoginForm from '../entry/login';
import RegisterForm from '../entry/register';
import { Error404 } from '../error/errors';
import Header from '../layout/header';
import { ProtectedRoute } from '../layout/protected';
import AnswerChallenge from '../challenge/answer';
import CreateChallenge from '../challenge/create';
import Search from '../search/search';
import SettingsPage from '../settings/settings';
import SplashPage from './SplashPage';

const App: React.FC = () => {
    return (
        <Switch>
            <Route exact path='/' component={SplashPage} />
            <Route exact path='/*' component={MainApp} />
        </Switch>
    );
}

const MainApp: React.FC = () => (
    <div>
        <Header />
        <main>
            <Switch>
                <Route exact path='/login' component={LoginForm} />
                <Route exact path='/register' component={RegisterForm} />
                <ProtectedRoute exact path='/challenge/:id' component={AnswerChallenge} />
                <ProtectedRoute exact path='/domain/:id' component={Domain} />
                <ProtectedRoute exact path='/search' component={Search} />
                <ProtectedRoute exact path='/dashboard' component={UserDashboard} />
                <ProtectedRoute exact path='/admin/dashboard' component={AdminDashboard} adminOnly={true} />
                <ProtectedRoute exact path='/admin/dashboard/challenge/create' component={CreateChallenge} adminOnly={true} />
                <ProtectedRoute exact path='/admin/dashboard/domain/create' component={CreateDomain} adminOnly={true} />
                <ProtectedRoute exact path='/admin/dashboard/challenge/:id' component={DashboardChallenge} adminOnly={true} />
                <ProtectedRoute exact path='/admin/dashboard/:id' component={DomainDashboard} adminOnly={true} />
                <ProtectedRoute exact path='/settings' component={SettingsPage} />
                <Route path='/*' component={Error404} />
            </Switch>
        </main>
    </div>
);

export default App;
