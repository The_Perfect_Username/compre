import React from 'react';
import { Link, withRouter } from 'react-router-dom';
import FeatureBrowseQuestions from '../../images/feature_browse_questions.png';
import Logo from '../../images/logo_white.svg';
import { userIsLoggedIn } from '../../lib/auth';

const SplashPage = (props: any): JSX.Element => {
    const isLoggedIn = userIsLoggedIn();

    if (isLoggedIn) {
        props.history.push(`/dashboard`);
    }

    const Header = () => {
        return (
            <header>
                <div className="container d-flex spaced">
                    <img src={Logo} alt='Kassess Logo' />
                    <nav className="header-menu">
                        <a href="#product">Product</a>
                        <a href="/login">Login</a>
                    </nav>
                </div>
            </header>
        );
    }

    const HeaderSection = () => (
        <div className="main-header">
            <div className="main-header-content">
                <h1 className="main-title">
                    Built for high achievers
                </h1>
            </div>
        </div>
    );

    const Product = () => (
        <div id="product" className="section">
            <div className="container d-flex center spaced feature">
                <div>
                    <div className="mb-30">
                        <h2 className="product-title">
                            Instant Assessment
                        </h2>
                        <p className="product-text">
                            Short-answer challenges are assessed instantly as Strong, Good, or Weak understanding of a topic.
                        </p>
                    </div>
                    <div className="mb-30">
                        <h2 className="product-title">
                            For Exams and Interviews
                        </h2>
                        <p className="product-text">
                            Challenges are designed to improve your understanding of your chosen topic which can be helpful for exams and job interviews.
                        </p>
                    </div>
                    <div className="mb-30">
                        <h2 className="product-title">
                            Constantly Improving AI
                        </h2>
                        <p className="product-text">
                            The more challenges the community completes, the smarter and more accurate the AI gets.
                        </p>
                    </div>
                </div>
                <div>
                    <img src={FeatureBrowseQuestions} alt="Preview of feature" />
                </div>
            </div>
        </div>
    );

    const Demo = () => (
        <div className="signup-section d-flex justify-content-center">
            <div className="container">
                <div className="signup-information">
                    <h2>Start Your Assessment</h2>
                    <h3>Register your account now to start assessing your knowledge</h3>
                </div>
                <div className="signup">
                    <Link to="/register">
                        <div className="signup-link-item">
                            Get Started
                        </div>
                    </Link>
                </div>
            </div>
        </div>
    );

    const Introduction = () => (
        <div className="section">
            <div className="container">
                <h2 className="section-title">What is Kassess?</h2>
                <p className="std-text">
                    Kassess is a platform that assesses your knowledge and understanding of specific fields by challenging you with short-answer questions related to that field.
                </p>
                <p className="std-text">
                    Whether if you're studying for an exam, preparing for an interview, or simply looking to get that promotion, X will be able to score how well you understand those topics.
                </p>
            </div>
        </div>
    );

    const Footer = () => (
        <footer className="d-flex center">
            <div className="container d-flex center justify-content-center">
                <Link to="/">
                    Privacy Policy
                </Link>
            </div>
        </footer>
    );

    return (
        <div id="splash-page">
            <Header />
            <main>
                <HeaderSection />
                <Introduction />
                <Product />
                <Demo />
                <Footer />
            </main>
        </div>
    );
};

export default withRouter(SplashPage);
