
import React from 'react';

export const NoResults = (props: ResultsProps) => {
    const { visible } = props;

    if (!visible) {
        return <></>;
    }

    return (
        <div className="no-results">
            <h4>
                No challenges found
            </h4>
            <ul>
                <li>This domain might not have any challenges yet</li>
                <li>The URL parameters might be incorrect</li>
                <li>There might be a connection issue which caused the server request to fail</li>
            </ul>
        </div>
    );
};

export const NoDomains = (props: ResultsProps) => {
    const { visible } = props;

    if (!visible) {
        return <></>;
    }

    return (
        <div className="no-results">
            <h4>
                No domains found
            </h4>
            <ul>
                <li>There might be a connection issue which caused the server request to fail</li>
            </ul>
        </div>
    );
};

export const ChallengeDoesNotExist = () => (
    <div className="container pt-40 text-center">
        <h2>Challenge does not exist</h2>
        <span>This might be because of a network issue or an incorrect url.</span>
    </div>
);

export const Error404 = () => (
    <div className="container pt-40 text-center">
        <h2>404 page not found</h2>
        <span>The requested page could not be found. Please recheck the url.</span>
    </div>
);

type ResultsProps = {
    visible: boolean;
};
