import axios from 'axios';
import queryString from 'query-string';
import React, { useEffect, useState, useCallback } from 'react';
import { withRouter } from 'react-router-dom';
import { API_URL } from '../../config/globals';
import Challenge from '../domains/challenge';
import { LoadingResults } from '../layout/tiles';
import { NoResults } from '../error/errors';
import { handleError } from '../../lib/error';

const Search = (props: any): JSX.Element => {
    const [error, setError] = useState<string>('');
    const [query, setQuery] = useState<string>('');
    const [results, setResults] = useState<Results[]>([]);
    const [loading, setLoading] = useState<boolean>(false);

    const searchQuery = props.location.search;

    window.scroll(0, 0);

    const fetchResults = useCallback(() => {
        setResults([]);
        setLoading(true);

        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        axios.get(`${API_URL}/search?search_query=${query}`, { headers })
            .then(response => {
                const { data } = response;
                setResults(data);
                setLoading(false);
            })
            .catch(error => {
                const { response } = error;
                const errorResponse = handleError(response);

                console.error(errorResponse.error);

                setError(errorResponse.error);
                setLoading(false);
            });
    }, [query]);

    const updateSearch = useCallback((query: string) => {
        const searchTerms = queryString.parse(query);
        const { search_query: searchQueryString } = searchTerms;

        if (searchQueryString) {
            setQuery(String(searchQueryString));
            fetchResults();
        } else {
            setQuery('');
            setResults([]);
        }
    }, [fetchResults]);

    useEffect(() => {
        updateSearch(searchQuery);
    }, [searchQuery, updateSearch]);

    const resultElements = results.map((result, index) => {
        const {
            answer,
            id,
            challenge,
            status,
        } = result;

        return <Challenge key={index} id={id} challenge={challenge} answer={answer} status={status} />
    });

    const showNoResults = (results.length === 0 && !loading) || !!error;

    return (
        <div className="container pt-40">
            <div className="content">
                {resultElements}
                <LoadingResults loading={loading} />
                <NoResults visible={showNoResults} />
            </div>
        </div>
    );
};

type Results = {
    id: string;
    answer: string;
    challenge: string;
    status: string;
}

export default withRouter(Search);
