import React, { Component, ChangeEvent, FormEvent } from 'react';
import { Search } from '@material-ui/icons';

export default class SearchBar extends Component<ISearchBar> {
    render() {
        const {
            onChange,
            onSubmit,
            placeholder,
        } = this.props;

        return (
            <form onSubmit={onSubmit}>
                <div className="search-bar">
                    <input
                        placeholder={placeholder}
                        type="text"
                        maxLength={100}
                        onChange={onChange}
                    />
                    <button className="no-btn">
                        <Search />
                    </button>
                </div>
            </form>
        );
    };
};

interface ISearchBar {
    onChange(e: ChangeEvent): void;
    onSubmit(e: FormEvent): void;
    placeholder: string;
};
