import axios from 'axios';
import React, { ChangeEvent, useState } from 'react';
import { withRouter } from 'react-router';
import { API_URL } from '../../config/globals';
import { LabelWithCounter, FormGroup, ErrorMessage, Button } from '../form/form';
import { userIsLoggedIn } from '../../lib/auth';
import { handleError } from '../../lib/error';

const LoginForm = (props: any): JSX.Element => {
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [error, setError] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);

    if (userIsLoggedIn()) {
        props.history.push('/dashboard');
    }

    const onEmailChange = (e: ChangeEvent): void => {
        const input: any = e.currentTarget;
        const email = input.value;

        setEmail(email);
    }

    const onPasswordChange = (e: ChangeEvent): void => {
        const input: any = e.currentTarget;
        const password = input.value;

        setPassword(password);
    }

    const onSubmit = (e: any): void => {
        e.preventDefault();

        setLoading(true);

        axios.post(`${API_URL}/login`, {
            email,
            password,
        })
        .then(response => {
            const { data } = response;
            const localStorage = window.localStorage;

            const userData = {
                id: data.id,
                name: data.name,
                email: data.email,
                is_admin: data.is_admin,
            };

            if (!data.is_admin) {
                delete userData.is_admin;
            }

            localStorage.setItem('token', data.token);
            localStorage.setItem('user', JSON.stringify(userData));

            window.location.replace('/dashboard');
        })
        .catch(error => {
            const { response } = error;
            const errorResponse = handleError(response);
            setError(errorResponse.error);
            setLoading(false);
        });
    }

    return (
        <div className="container min-height-100 d-flex center">
            <form className="entry-form" onSubmit={onSubmit}>
                <FormGroup>
                    <h1 className="entry-title">Welcome Back</h1>
                </FormGroup>
                <FormGroup>
                    <LabelWithCounter maxLength={75} value={email}>
                        Email
                    </LabelWithCounter>
                    <input onChange={onEmailChange} type="email" maxLength={75} />
                </FormGroup>
                <FormGroup>
                    <label>Password</label>
                    <input onChange={onPasswordChange} type="password" />
                </FormGroup>
                <FormGroup>
                    <Button className="btn btn-default stretched" loading={loading}>
                        Login to your Account
                    </Button>
                    <ErrorMessage message={error} />
                </FormGroup>
            </form>
        </div>
    );
};

export default withRouter(LoginForm);
