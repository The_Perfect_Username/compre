import axios from 'axios';
import React, { ChangeEvent, useState } from 'react';
import { withRouter } from 'react-router';
import { API_URL } from '../../config/globals';
import {
    LabelWithCounter,
    LabelWithPasswordStrength,
    FormGroup,
    ErrorMessage,
    Button,
} from '../form/form';
import { userIsLoggedIn } from '../../lib/auth';
import { handleError } from '../../lib/error';

type Errors = {
    name: string;
    email: string;
    password: string;
    error: string;
}

const RegisterForm = (props: any): JSX.Element => {
    const [name, setName] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [loading, setLoading] = useState<boolean>(false);
    const [errors, setErrors] = useState<Errors>({
        name: '',
        email: '',
        password: '',
        error: '',
    });

    if (userIsLoggedIn()) {
        props.history.push('/dashboard');
    }

    const onNameChange = (e: ChangeEvent): void => {
        const input: any = e.currentTarget;
        const name = input.value;

        setName(name);
    }

    const onEmailChange = (e: ChangeEvent): void => {
        const input: any = e.currentTarget;
        const email = input.value;

        setEmail(email);
    }

    const onPasswordChange = (e: ChangeEvent): void => {
        const input: any = e.currentTarget;
        const password = input.value;

        setPassword(password);
    }

    const onSubmit = (e: any): void => {
        e.preventDefault();

        setLoading(true);

        axios.post(`${API_URL}/user`, {
            name,
            email,
            password,
        })
        .then(response => {
            const { data } = response;
            const localStorage = window.localStorage;

            localStorage.setItem('token', data.token);
            localStorage.setItem('user', JSON.stringify({
                id: data.id,
                name: data.name,
                email: data.email,
            }));

            window.location.replace('/');
        })
        .catch(error => {
            const { response } = error;
            const errorResponse = handleError(response);

            setErrors({ ...errorResponse });
            setLoading(false);
        });
    }

    return (
        <div className="container min-height-100 d-flex center">
            <form className="entry-form" onSubmit={onSubmit}>
                <FormGroup>
                    <h1 className="entry-title">Registration</h1>
                </FormGroup>
                <FormGroup>
                    <LabelWithCounter maxLength={50} value={name}>
                        Name
                    </LabelWithCounter>
                    <input onChange={onNameChange} type="text" maxLength={50} />
                    <ErrorMessage message={errors.name} />
                </FormGroup>
                <FormGroup>
                    <LabelWithCounter maxLength={75} value={email}>
                        Email
                    </LabelWithCounter>
                    <input onChange={onEmailChange} type="email" maxLength={75} />
                    <ErrorMessage message={errors.email} />
                </FormGroup>
                <FormGroup>
                    <LabelWithPasswordStrength value={password}>
                        Password
                    </LabelWithPasswordStrength>
                    <input onChange={onPasswordChange} type="password" />
                    <ErrorMessage message={errors.password} />
                </FormGroup>
                <FormGroup>
                    <Button className="btn btn-default stretched" loading={loading}>
                        Create your Account
                    </Button>
                    <ErrorMessage message={errors.error} />
                </FormGroup>
                <FormGroup>
                    <span className="terms-and-conditions">By continuing you agree to our privacy policy and terms of service.</span>
                </FormGroup>
            </form>
        </div>
    );
};

export default withRouter(RegisterForm);
