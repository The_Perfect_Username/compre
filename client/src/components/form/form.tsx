import { Warning } from '@material-ui/icons';
import React, { useEffect, useState } from 'react';
import BeatLoader from 'react-spinners/BeatLoader';
import MoonLoader from 'react-spinners/MoonLoader';

export const LabelWithCounter = (props: LabelWithCounter) => {
    const [count, setCount] = useState<number>(0);

    const { maxLength, value, children } = props;

    useEffect(() => {
        const lengthOfInput = value
            ? maxLength - value.length
            : maxLength;

        setCount(lengthOfInput);
    }, [value, maxLength]);

    return (
        <div className="label-with-counter">
            <label>{children}</label>
            <span className="counter">{count}</span>
        </div>
    );
};

export const LabelWithPasswordStrength = (props: LabelWithPasswordStrength) => {
    const [strength, setStrength] = useState<PasswordStrength>('weak');

    const { value, children } = props;

    useEffect(() => {
        let strengthValue: PasswordStrength = 'weak';
        const passwordLength = value.length;

        if (passwordLength >= 6 && passwordLength <= 11) {
            strengthValue = 'good';
        } else if (passwordLength >= 12) {
            strengthValue = 'strong';
        }

        setStrength(strengthValue);
    }, [value.length]);

    return (
        <div className="label-with-counter">
            <label>{children}</label>
            <span className={`strength-indicator ${strength}`}></span>
        </div>
    );
};

export const FormGroup = (props: FormGroup): JSX.Element => {
    const { className, children } = props;
    const __className = className || 'form-group';

    return (
        <div className={__className}>
            {children}
        </div>
    );
};

export const ErrorMessage = (props: ErrorMessage): JSX.Element => {
    const { message } = props;

    if (!message || message === '') {
        return <></>;
    }

    return (
        <div className="error-message d-flex">
            <div className="error-icon d-flex center">
                <Warning fontSize="small" />
            </div>
            <div className="d-flex center">
                <span>{message}</span>
            </div>
        </div>
    );
};

export const Button = (props: any) => {
    const {
        children,
        className,
        loading,
        disabled,
        onClick,
    } = props;

    const override = `
        padding: 1px;
        display: flex;
        justify-content: center;
        align-items: center;
    `;

    const buttonContent = loading
    ? <BeatLoader css={override} color="#fff" sizeUnit="px" size={10} />
    : children;

    return (
        <button className={className} onClick={onClick} disabled={disabled || loading}>
            {buttonContent}
        </button>
    );
};

export const FormLoader = (props: any) => {
    const { loading, children } = props;

    if (loading) {
        return (
            <div className="d-flex center justify-content-center mt-40">
                <MoonLoader color="#000" sizeUnit="px" size={30} />
            </div>
        );
    }

    return children;
}

type PasswordStrength = 'weak' | 'good' | 'strong';

type LabelWithCounter = {
    value: string | '';
    maxLength: number;
    children?: any;
};

type LabelWithPasswordStrength = {
    value: string | '';
    children: any;
};

type ErrorMessage = {
    message: string | '' | null;
};

type FormGroup = {
    className?: string;
    children: any;
};
