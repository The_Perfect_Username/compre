import React from 'react';
import { Link } from 'react-router-dom';
import Status from '../challenge/status';

const Challenge = (props: any): JSX.Element => {
    const {
        answer,
        id,
        challenge,
        status,
    } = props;

    return (
        <Link to={`/challenge/${id}`} className="no-decoration">
            <div className="challenge">
                <Status
                    defaultLabel="unanswered"
                    label={`${status} understanding`}
                    status={status}
                />
                <h2>{challenge}</h2>
                {answer && (
                    <div className="answer">
                        <p>{answer}</p>
                    </div>
                )}
            </div>
        </Link>
    );
};

export default Challenge;