import axios from 'axios';
import React, { Component, ChangeEvent } from 'react';
import { withRouter } from 'react-router-dom';
import { FormGroup, LabelWithCounter, ErrorMessage, Button } from '../form/form';
import { API_URL } from '../../config/globals';
import { handleError } from '../../lib/error';

class CreateDomain extends Component<any> {
    state = {
        domain: '',
        loading: false,
        error: '',
    }

    onDomainChange = (e: ChangeEvent) => {
        const input: any = e.currentTarget;
        const domain = input.value;
        this.setState({ domain });
    }

    onSubmit = (e: any) => {
        e.preventDefault();
        this.setState({ loading: true });

        const { domain } = this.state;
        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        axios.post(`${API_URL}/domain`, { domain }, { headers })
            .then(response => {
                const { data } = response;
                const { id } = data;
                this.props.history.push(`/domain/${id}`);
            })
            .catch(error => {
                const { response } = error;
                const errorResponse = handleError(response);
                this.setState({ ...errorResponse , loading: false });
            });
    }

    render() {
        return (
            <div id="create-challenge" className="container pt-40 d-flex spaced">
                <div className="content">
                    <form onSubmit={this.onSubmit}>
                        <h2>Create Domain</h2>
                        <FormGroup>
                            <LabelWithCounter maxLength={100} value={this.state.domain}>
                                Domain Name
                            </LabelWithCounter>
                            <input className="form-control" maxLength={100} onChange={this.onDomainChange} />
                            <ErrorMessage message={this.state.error} />
                        </FormGroup>
                        <FormGroup>
                            <div className="d-flex reverse space-buttons">
                                <Button
                                    className="btn btn-default"
                                    loading={this.state.loading}
                                >
                                    Submit
                                </Button>
                            </div>
                        </FormGroup>
                    </form>
                </div>
            </div>
        );
    }
};

export default withRouter(CreateDomain);
