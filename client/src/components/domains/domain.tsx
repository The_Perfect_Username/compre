import axios from 'axios';
import React, { Component } from 'react';
import { withRouter } from 'react-router';
import Challenge from './challenge';
import { API_URL } from '../../config/globals';
import { LoadingResults } from '../layout/tiles';
import { NoResults } from '../error/errors';
import { handleError } from '../../lib/error';
import BreadCrumbs from '../layout/breadcrumbs';
import LoadMoreButton from '../layout/loadmore';

class Domain extends Component<any> {
    state = {
        domain: '',
        challenges: [],
        loading: false,
        offset: 0,
        reached_limit: false,
    };

    componentDidMount() {
        const domainId = this.props.match.params.id;
        const domain = domainId.replace(/_/g, ' ');

        this.setState({ domain });

        this.fetchResults();
    }

    fetchResults = () => {
        const { challenges, offset } = this.state;
        const domainId = this.props.match.params.id;
        const limit = 10;
        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        this.setState({ loading: true });

        axios.get(`${API_URL}/challenge/fetch?id=${domainId}&limit=${limit}&offset=${offset}`, { headers })
            .then((response: any) => {
                const { data } = response;
                const results = [...challenges, ...data];

                this.setState({
                    challenges: results,
                    loading: false,
                    offset: offset + limit,
                    reached_limit: data.length !== limit,
                });
            })
            .catch((error: any) => {
                const { response } = error;
                const errorResponse = handleError(response);

                this.setState({ ...errorResponse, loading: false });
            });
    }

    loadMoreButton = () => {
        const {
            challenges,
            loading,
            reached_limit: reachedLimit,
        } = this.state;

        if (challenges.length === 0) {
            return <></>;
        }

        return (
            <LoadMoreButton
                className="btn btn-default"
                loading={loading}
                onClick={this.fetchResults}
                reachedLimit={reachedLimit}
            />
        );
    };

    renderChallenges = ({ challengeComponents }: any): JSX.Element => {
        const { challenges, loading } = this.state;
        const showLoadingTiles = loading && challenges.length === 0;
        const showNoResultsMessage = challenges.length === 0 && !loading;

        if (showLoadingTiles) {
            return (
                <div>
                    <LoadingResults loading={showLoadingTiles} />
                </div>
            );
        }

        if (showNoResultsMessage) {
            return <NoResults visible={showNoResultsMessage} />;
        }

        return (
            <div>
                {challengeComponents}
                <this.loadMoreButton />
            </div>
        );
    }

    render() {
        const { challenges, domain } = this.state;

        const challengeComponents = challenges.map((challengeData, index) => {
            const {
                answer,
                id,
                challenge,
                status,
            } = challengeData;

            return <Challenge key={index} id={id} challenge={challenge} answer={answer} status={status} />
        });

        return (
            <div className="container pt-40 pb-40">
                <div className="content">
                    <BreadCrumbs crumbs={['Domains', domain]} />
                    <this.renderChallenges challengeComponents={challengeComponents} />
                </div>
            </div>
        );
    }
};

export default withRouter(Domain);
