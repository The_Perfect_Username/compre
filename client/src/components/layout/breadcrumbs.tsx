import React from 'react';
import { KeyboardArrowRight } from '@material-ui/icons';

const BreadCrumbs = (props: IBreadCrumbs) => {
    const { crumbs } = props;
    const elements = crumbs.map((crumb: any, index: number) => {
        if (index === 0) {
            return <span key={index} className="crumb capitalise">{crumb}</span>
        }

        return (
            <span key={index}>
                <KeyboardArrowRight className="crumb" />
                <span className="crumb capitalise">{crumb}</span>
            </span>
        )
    });

    return (
        <div className="bread-crumbs">
            {elements}
        </div>
    );
};

interface IBreadCrumbs {
    crumbs: string[];
};

export default BreadCrumbs;