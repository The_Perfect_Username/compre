import React from 'react';
import { Button } from '../form/form';

const LoadMoreButton = (props: any) => {
    const {
        className,
        loading,
        onClick,
        reachedLimit,
    } = props;

    if (reachedLimit) {
        return (
            <div className="d-flex justify-content-center">
                <span>No more results!</span>
            </div>
        );
    }

    return (
        <div className="d-flex justify-content-center">
            <Button
                className={className}
                loading={loading}
                onClick={onClick}
            >
                Load More
            </Button>
        </div>
    );
}

export default LoadMoreButton;