import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { getAccount } from '../../lib/auth';

export default class UserDropDown extends Component<any> {
    private dropdownBtn: any;

    constructor(props: any) {
        super(props);
        this.dropdownBtn = React.createRef();
    }

    state = {
        is_admin: false,
        active: false,
        initial: '',
        name: '',
        email: '',
    }

    componentDidMount() {
        try {
            const userDetails = getAccount();
            const { name, email, is_admin } = userDetails;
            const initial = name[0];

            this.setState({ initial, name, email, is_admin });
        } catch (e) {
            console.error(e);
        } finally {
            window.addEventListener('click', this.closeMenu);
        }
    }

    closeMenu = (e: Event) => {
        const { active } = this.state;
        const { current } = this.dropdownBtn;

        if (current) {
            const constainsCurrentTarget = current.contains(e.target);

            if (active && !constainsCurrentTarget) {
                this.setState({ active: false });
            }
        }

    }

    toggleMenu = () => {
        this.setState({ active: !this.state.active });
    }

    logout = (e: any) => {
        e.preventDefault();
        window.localStorage.removeItem('user');
        window.localStorage.removeItem('token');
        window.location.replace('/login');
    }

    render() {
        const { email, name, is_admin: isAdmin } = this.state;

        return (
            <div className="dropdown">
                <div className="dropdown-btn" onClick={this.toggleMenu} ref={this.dropdownBtn}>
                    <span>{this.state.initial}</span>
                </div>
                {this.state.active &&
                    <div className="dropdown-body">
                        <div className="current-user">
                            <span className="name">
                                {name}
                            </span>
                            <span className="email">
                                {email}
                            </span>
                        </div>
                        {isAdmin &&
                            <Link to='/admin/dashboard'>Admin</Link>
                        }
                        <Link to='/dashboard'>Dashboard</Link>
                        <Link to='/settings'>Settings</Link>
                        <Link to='/' onClick={this.logout}>Logout</Link>
                    </div>
                }
            </div>
        );
    }
};
