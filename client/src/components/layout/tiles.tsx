import React, { useEffect, useState } from 'react';

export const Tile = (props: TileProps): JSX.Element => {
    const [loading, setLoading] = useState<boolean>(true);
    const { children, type, loading: isLoading } = props;

    useEffect(() => {
        setLoading(isLoading);
    }, [isLoading]);

    const classes: any = {
        title: 'loading-tile title-tile',
        status: 'loading-tile status-tile',
    };

    if (loading && classes.hasOwnProperty(type)) {
        return <div className={classes[type]}></div>;
    }

    return <>{children}</>
}

export const ResultTile = (): JSX.Element => (
    <div className="loading-tile result-tile"></div>
);

export const LoadingResults = (props: LoadingResultsProps): JSX.Element => {
    const [loading, setLoading] = useState<boolean>(true);
    const { loading: isLoading } = props;

    useEffect(() => {
        setLoading(isLoading);
    }, [isLoading]);

    if (!loading) {
        return <></>;
    }

    return (
        <>
            <ResultTile />
            <ResultTile />
            <ResultTile />
        </>
    );
};

type TileProps = {
    loading: boolean;
    type: string;
    children?: any;
}

type LoadingResultsProps = {
    loading: boolean;
}
