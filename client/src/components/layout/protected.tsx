import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { isAdmin, userIsLoggedIn } from '../../lib/auth';

export const ProtectedRoute = ({ component: Component, ...rest}: any) => {
    const { adminOnly } = rest;
    const isLoggedIn = userIsLoggedIn();
    const userIsAdmin = isAdmin();

    if (!!adminOnly) {
        return <Route {...rest} render={(props) => (
            userIsAdmin
            ? <Component {...props} />
            : <Redirect to="/register" />
        )} />;
    }

    return <Route {...rest} render={(props) => (
        isLoggedIn
        ? <Component {...props} />
        : <Redirect to="/register" />
    )} />;
}
