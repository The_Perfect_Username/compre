import React, { Component, ChangeEvent, FormEvent } from 'react';
import { Link, withRouter } from 'react-router-dom';
import UserDropDown from './dropdown';
import SearchBar from '../search/searchbar';
import Logo from '../../images/logo_blue.svg';
import { userIsLoggedIn } from '../../lib/auth';

class Header extends Component<any> {
    state = {
        query: '',
    }

    onChange = (e: ChangeEvent): void => {
        const target: any = e.currentTarget;

        if (target) {
            const query: string = target.value;
            this.setState({ query });
        }
    }

    onSubmit = (e: FormEvent): void => {
        e.preventDefault();
        const { query } = this.state;
        this.props.history.push(`/search?search_query=${query.split(' ').join('+')}`);
    }

    render() {
        const isLoggedIn = userIsLoggedIn();

        return (
            <header>
                <div className="container d-flex spaced center">
                    <Link to="/">
                        <img src={Logo} alt='Kassess Logo' />
                    </Link>
                    <div className="d-flex center">
                        <SearchBar
                            onChange={this.onChange}
                            onSubmit={this.onSubmit}
                            placeholder="Search..."
                            {...this.props}
                        />
                        {!isLoggedIn &&
                            <EntryButtons />
                        }
                        {isLoggedIn &&
                            <UserDropDown />
                        }
                    </div>
                </div>
            </header>
        );
    };
};

const EntryButtons: React.FC = () => (
    <div className="entry-buttons d-flex center">
        <Link to="/login" className="btn btn-default">Login</Link>
        <Link to="/register" className="btn btn-link">Register</Link>
    </div>
);

export default withRouter(Header);
