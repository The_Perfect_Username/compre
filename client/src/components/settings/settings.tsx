import axios from 'axios';
import React, { ChangeEvent, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import {
    FormGroup,
    LabelWithCounter,
    LabelWithPasswordStrength,
    Button,
    ErrorMessage,
} from '../form/form';
import { API_URL } from '../../config/globals';
import { handleError } from '../../lib/error';

type BasicErrors = {
    name: string;
    email: string;
    error: string;
}

type PasswordErrors = {
    new_password: string;
    confirm_password: string;
    current_password: string;
    error: string;
}

const SettingsPage = (): JSX.Element => (
    <div className="container pt-40 d-flex spaced">
        <div className="content w-500">
            <BasicInformationForm />
            <ChangePasswordForm />
        </div>
    </div>
);

const BasicInformationForm = () => {
    const [name, setName] = useState<string>('');
    const [email, setEmail] = useState<string>('');
    const [errors, setErrors] = useState<BasicErrors>({
        name: '',
        email: '',
        error: '',
    });
    const [loading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        const userDetails = window.localStorage.getItem('user');

        if (userDetails) {
            const userData = JSON.parse(userDetails);
            const { name, email } = userData;
            setName(name);
            setEmail(email);
        }
    }, []);

    const onSubmit = (e: any) => {
        e.preventDefault();
        setLoading(true);

        const data = { email, name };
        const headers = { 'Authorization': `Bearer ${window.localStorage.getItem('token')}` };

        axios.put(`${API_URL}/user`, data, { headers })
            .then((response: any) => {
                const { data } = response;
                const localStorage = window.localStorage;

                const userData = {
                    id: data.id,
                    name: data.name,
                    email: data.email,
                    is_admin: data.is_admin,
                };

                if (!data.is_admin) {
                    delete userData.is_admin;
                }

                localStorage.setItem('user', JSON.stringify(userData));
                setLoading(false);
                setErrors({ name: '', email: '', error: '' });
            })
            .catch((error: any) => {
                const { response } = error;
                const errorResponse = handleError(response);
                setErrors( { ...errorResponse });
                setLoading(false);
            });
    }

    const handleName = (e: ChangeEvent) => {
        const input: any = e.currentTarget;
        const value: string = input.value;

        setName(value);
    }

    const handleEmail = (e: ChangeEvent) => {
        const input: any = e.currentTarget;
        const value: string = input.value;

        setEmail(value);
    }

    return (
        <div>
            <form onSubmit={onSubmit}>
                <h4>Basic Information</h4>
                <FormGroup>
                    <LabelWithCounter
                        value={name}
                        maxLength={100}
                    >
                        Name
                    </LabelWithCounter>
                    <input
                        onChange={handleName}
                        defaultValue={name}
                        type="text"
                        maxLength={100}
                        className="form-control"
                    />
                    <ErrorMessage message={errors.name} />
                </FormGroup>
                <FormGroup>
                    <LabelWithCounter
                        value={email}
                        maxLength={100}
                    >
                        Email
                    </LabelWithCounter>
                    <input
                        onChange={handleEmail}
                        defaultValue={email}
                        type="email"
                        maxLength={100}
                        className="form-control"
                    />
                    <ErrorMessage message={errors.email} />
                </FormGroup>
                <FormGroup>
                    <div className="d-flex reverse space-buttons">
                        <Button
                            className="btn btn-default"
                            loading={loading}
                        >
                            Update
                        </Button>
                    </div>
                    <ErrorMessage message={errors.error} />
                </FormGroup>
            </form>
        </div>
    );
}

const ChangePasswordForm = (): JSX.Element => {
    const [newPassword, setNewPassword] = useState<string>('');
    const [confirmPassword, setConfirmPassword] = useState<string>('');
    const [currentPassword, setCurrentPassword] = useState<string>('');
    const [errors, setErrors] = useState<PasswordErrors>({
        new_password: '',
        confirm_password: '',
        current_password: '',
        error: '',
    });
    const [loading, setLoading] = useState<boolean>(false);

    const handleNewPassword = (e: ChangeEvent) => {
        const input: any = e.currentTarget;
        const value: string = input.value;

        setNewPassword(value);
    }

    const handleConfirmPassword = (e: ChangeEvent) => {
        const input: any = e.currentTarget;
        const value: string = input.value;

        setConfirmPassword(value);
    }

    const handleCurrentPassword = (e: ChangeEvent) => {
        const input: any = e.currentTarget;
        const value: string = input.value;

        setCurrentPassword(value);
    }

    const onSubmit = (e: any) => {
        e.preventDefault();

        setLoading(true);

        const data = {
            new_password: newPassword,
            current_password: currentPassword,
            confirm_password: confirmPassword,
        };

        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        axios.put(`${API_URL}/user/password`, data, { headers })
            .then(() => {
                setLoading(false);
                setNewPassword('');
                setConfirmPassword('');
                setCurrentPassword('');
                setErrors({
                    new_password: '',
                    confirm_password: '',
                    current_password: '',
                    error: '',
                });
            })
            .catch((error: any) => {
                const { response } = error;
                const errorResponse = handleError(response);
                setErrors({ ...errorResponse });
                setLoading(false);
            });
    }

    return (
        <div className="mt-40">
            <form onSubmit={onSubmit}>
                <h4>Change Password</h4>
                <FormGroup>
                    <LabelWithPasswordStrength
                        value={newPassword}
                    >
                        New Password
                    </LabelWithPasswordStrength>
                    <input
                        value={newPassword}
                        onChange={handleNewPassword}
                        type="password"
                        maxLength={100}
                        className="form-control"
                    />
                    <ErrorMessage message={errors.new_password} />
                </FormGroup>
                <FormGroup>
                    <LabelWithPasswordStrength
                        value={confirmPassword}
                    >
                        Confirm Password
                    </LabelWithPasswordStrength>
                    <input
                        value={confirmPassword}
                        onChange={handleConfirmPassword}
                        type="password"
                        maxLength={100}
                        className="form-control"
                    />
                    <ErrorMessage message={errors.confirm_password} />
                </FormGroup>
                <FormGroup>
                    <LabelWithPasswordStrength
                        value={currentPassword}
                    >
                        Current Password
                    </LabelWithPasswordStrength>
                    <input
                        value={currentPassword}
                        onChange={handleCurrentPassword}
                        type="password"
                        maxLength={100}
                        className="form-control"
                    />
                    <ErrorMessage message={errors.current_password} />
                </FormGroup>
                <FormGroup>
                    <div className="d-flex reverse space-buttons">
                        <Button
                            className="btn btn-default"
                            loading={loading}
                        >
                            Update
                        </Button>
                    </div>
                    <ErrorMessage message={errors.error} />
                </FormGroup>
            </form>
        </div>
    );
}

export default withRouter(SettingsPage);
