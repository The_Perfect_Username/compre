import axios from 'axios';
import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { API_URL } from '../../config/globals';
import { NoDomains } from '../error/errors';
import { LoadingResults } from '../layout/tiles';
import { handleError } from '../../lib/error';
import { getAccount } from '../../lib/auth';

class Dashboard extends Component<any> {
    state = {
        name: '',
        loading: false,
        domains: [],
    }

    componentDidMount() {
        this.setUserDetails();

        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        this.setState({ loading: true });

        axios.get(`${API_URL}/domain/fetch`, { headers })
            .then((response) => {
                const { data } = response;
                this.setState({ domains: data, loading: false });
            })
            .catch((error) => {
                const { response } = error;
                const errorResponse = handleError(response);

                this.setState({ ...errorResponse, loading: false });
            });
    }

    setUserDetails = () => {
        try {
            const userDetails = getAccount();
            const { name } = userDetails;

            this.setState({ name });
        } catch (e) {
            console.error(e);
        }
    }

    renderDomains = (): JSX.Element => {
        const { domains, loading } = this.state;

        const showNoResultsMessage = domains.length === 0 && !loading;

        if (loading) {
            return (
                <LoadingResults loading={loading} />
            );
        }

        if (showNoResultsMessage) {
            return <NoDomains visible={showNoResultsMessage} />;
        }

        const domainArray = domains.map((domain: any, index) => <Domain id={domain.id} name={domain.name} key={index} />);

        return <>{domainArray}</>;
    }

    render() {
        const { name } = this.state;

        return (
            <div>
                <div className="bg-gray">
                    <div className="container pt-40 pb-40">
                        <h2>Hello {name}</h2>
                    </div>
                </div>
                <div className="container pt-40 pb-40">
                    <h3>Domains</h3>
                    <div className="grid">
                        <this.renderDomains />
                    </div>
                </div>
            </div>
        );
    }
}

const Domain = (props: DomainProps): JSX.Element => {
    const { id, name } = props;
    return (
        <div className="grid-tile">
            <Link to={`/domain/${id}`} className="no-decoration">
                <div className="domain">
                    <h2>{name}</h2>
                </div>
            </Link>
        </div>
    );
}

interface DomainProps {
    id: string;
    name: string;
}

export default withRouter(Dashboard);
