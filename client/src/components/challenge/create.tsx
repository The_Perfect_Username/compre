import axios from 'axios';
import queryString from 'query-string';
import React, { Component, ChangeEvent } from 'react';
import { withRouter } from 'react-router-dom';
import { FormGroup, LabelWithCounter, ErrorMessage, Button } from '../form/form';
import { API_URL } from '../../config/globals';
import { handleError } from '../../lib/error';

class CreateChallenge extends Component<any> {
    state = {
        domain: '',
        challenge: '',
        file: '',
        domains: [],
        loading: false,
        errors: {
            domain: '',
            challenge: '',
            file: '',
            error: '',
        },
    }

    componentDidMount() {
        const query = this.props.location.search;

        if (query) {
            const searchTerms = queryString.parse(query);
            const { domain } = searchTerms;
            this.setState({ domain });
        }
    }

    componentWillMount() {
        axios.get(`${API_URL}/challenge/categories`)
            .then(res => {
                const { data } = res;
                this.setState({ domains: data });
            })
            .catch(err => console.error(err));
    }

    onDomainChange = (e: ChangeEvent) => {
        const input: any = e.currentTarget;
        const domain = input.value;
        this.setState({ domain });
    }

    onChallengeChange = (e: ChangeEvent) => {
        const input: any = e.currentTarget;
        const challenge = input.value;
        this.setState({ challenge });
    }

    onClick = (e: any) => {
        e.preventDefault();
        const fileUploadElement: HTMLElement | null = document.getElementById('fileUpload');

        if (fileUploadElement) {
            fileUploadElement.click();
        }
    }

    onFileChange = (e: any) => {
        const input = e.currentTarget;
        const file: File | Blob | null = input.files[0];

        this.setState({ file });
    }

    onSubmit = (e: any) => {
        e.preventDefault();

        const data = new FormData();

        const {
            domain,
            challenge,
            file,
        } = this.state;

        this.setState({ loading: true });

        data.set('domain', domain);
        data.set('challenge', challenge);
        data.append('file', file);

        axios({
            headers: {
                'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
            },
            url: `${API_URL}/challenge`,
            method: 'post',
            data,
        }).then(response => {
            const { data } = response;
            const { id } = data;
            this.props.history.push(`/admin/dashboard/challenge/${id}`);
        })
        .catch(error => {
            const { response } = error;
            const errorResponse = handleError(response);
            this.setState({ errors: { ...errorResponse }, loading: false });
        });
    }

    renderOptions = () => {
        const { domains } = this.state;

        if (!domains) {
            return <></>;
        }

        return domains.map((domain: any, index: any) => {
            const { id, name } = domain;

            return <option className="capitalise" key={index} value={id}>{name}</option>;
        });
    }

    render() {
        const { domain, file }: any = this.state;
        const fileInputLabel =  (!!file && file.name) || 'No file selected';

        return (
            <div id="create-challenge" className="container pt-40 d-flex spaced">
                <div className="content">
                    <form onSubmit={this.onSubmit}>
                        <FormGroup>
                            <label>Domain</label>
                            <select value={domain} className="form-control capitalise" onChange={this.onDomainChange}>
                                <option>Select...</option>
                                {this.renderOptions()}
                            </select>
                            <ErrorMessage message={this.state.errors.domain} />
                        </FormGroup>
                        <FormGroup>
                            <LabelWithCounter maxLength={100} value={this.state.challenge}>
                                Challenge
                            </LabelWithCounter>
                            <input className="form-control" maxLength={100} onChange={this.onChallengeChange} />
                            <ErrorMessage message={this.state.errors.challenge} />
                        </FormGroup>
                        <FormGroup>
                            <div className="file-upload">
                                <input type="file" id="fileUpload" onChange={this.onFileChange} />
                                <button onClick={this.onClick} className="btn">Upload Model</button>
                                <span>{fileInputLabel}</span>
                            </div>
                            <ErrorMessage message={this.state.errors.file} />
                        </FormGroup>
                        <FormGroup>
                            <div className="d-flex reverse space-buttons">
                                <Button
                                    className="btn btn-default"
                                    loading={this.state.loading}
                                >
                                    Submit
                                </Button>
                            </div>
                        </FormGroup>
                        <ErrorMessage message={this.state.errors.error} />
                    </form>
                </div>
            </div>
        );
    }
};

export default withRouter(CreateChallenge);
