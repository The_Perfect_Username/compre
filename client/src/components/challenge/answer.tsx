import { ArrowBack, ArrowForward, CheckCircleOutlined } from '@material-ui/icons';
import axios from 'axios';
import React, { FormEvent, useCallback, useEffect, useState } from 'react';
import { withRouter, Link } from 'react-router-dom';
import BeatLoader from 'react-spinners/BeatLoader';
import Status, { StatusLabels } from './status';
import { ChallengeDoesNotExist } from '../error/errors';
import {
    Button,
    FormGroup,
    LabelWithCounter,
    ErrorMessage,
    FormLoader,
} from '../form/form';
import { Tile } from '../layout/tiles';
import { API_URL } from '../../config/globals';
import { handleError } from '../../lib/error';

const AnswerChallenge = (props: any) => {
    const [answer, setAnswer] = useState<string>('');
    const [challenge, setChallenge] = useState<string>('');
    const [error, setError] = useState<string>('');
    const [challengeId, setChallengeId] = useState<string>('');
    const [domainId, setDomainId] = useState<string>('');
    const [domain, setDomain] = useState<string>('');
    const [status, setStatus] = useState<StatusLabels>('');
    const [answerLoading, setAnswerLoading] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const [challengeLoading, setChallengeLoading] = useState<boolean>(false);
    const [challengeExists, setChallengeExists] = useState<boolean>(true);
    const [sentAnswer, setSentAnswer] = useState<boolean>(false);
    const [nextChallenge, setNextChallenge] = useState<string | null>(null);
    const [prevChallenge, setPrevChallenge] = useState<string | null>(null);

    const headers = {
        'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
    };

    const getChallengeData = useCallback((challengeId: string) => {
        setChallengeLoading(true);

        // Request challenge information
        axios.get(`${API_URL}/challenge?id=${challengeId}`)
            .then(response => {
                const { data } = response;

                if (Object.values(data).length === 0) {
                    setChallengeExists(false);
                    setChallengeLoading(false);
                    return;
                }

                const {
                    id,
                    challenge,
                    domain,
                    domain_id: domainId,
                    next_challenge: nextChallengeId,
                    prev_challenge: prevChallengeId,
                } = data;

                setChallengeId(id);
                setChallenge(challenge);
                setDomain(domain);
                setDomainId(domainId);
                setNextChallenge(nextChallengeId);
                setPrevChallenge(prevChallengeId);
                setChallengeLoading(false);
            })
            .catch(error => {
                const { response } = error;
                const errorResponse = handleError(response);
                const { error: errorMessage } = errorResponse;

                setError(errorMessage);
                setChallengeLoading(false);
            });
    }, []);

    const getUserAnswerData = useCallback((challengeId: string) => {
        setAnswerLoading(true);

        // Request user's current answer
        axios.get(`${API_URL}/answer?id=${challengeId}`, { headers })
            .then(response => {
                const { data } = response;
                const { status, answer } = data;

                setStatus(status);
                setAnswer(answer);
                setAnswerLoading(false);
            })
            .catch(error => {
                const { response } = error;
                const errorResponse = handleError(response);
                const { error: errorMessage } = errorResponse;

                setError(errorMessage);
                setAnswerLoading(false);
            });
    }, [headers]);

    const resetState = () => {
        setAnswer('');
        setSentAnswer(false);
        setChallengeId('');
        setChallenge('');
        setDomain('');
        setDomainId('');
        setStatus('');
        setNextChallenge('');
        setPrevChallenge('');
    }

    useEffect(() => {
        const challengeIdFromUrl = props.match.params.id;

        if (challengeId !== challengeIdFromUrl) {
            resetState();
            setChallengeId(challengeIdFromUrl);
            getChallengeData(challengeIdFromUrl);
            getUserAnswerData(challengeIdFromUrl);
        }
    // eslint-disable-next-line
    }, [props.match.params.id]);


    const onSubmit = (e: FormEvent): void => {
        e.preventDefault();

        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        setLoading(true);

        createAnswer(answer, headers);
    }

    const createAnswer = (answer: string, headers: any): void => {
        const data = { answer, challenge_id: challengeId };

        axios.post(`${API_URL}/answer`, data, { headers })
            .then(response => {
                const { data } = response;
                const { status } = data;

                setStatus(status);
                setLoading(false);
                setSentAnswer(true);
                setError('');
            })
            .catch(error => {
                const { response } = error;
                const errorResponse = handleError(response);
                const { error: errorMessage } = errorResponse;

                setError(errorMessage);
                setLoading(false);
            });
    }

    const resultStatus: StatusLabels = status;
    const showContestAlert = sentAnswer && (status === 'weak' || status === 'good');

    const NextChallenge = (): JSX.Element | null => {
        if (nextChallenge) {
            return (
                <Link className="nav-btn d-flex center" to={`/challenge/${nextChallenge}`}>
                    <span>Next</span>
                    <ArrowForward />
                </Link>
            );
        }

        return null;
    }

    const PrevChallenge = (): JSX.Element => {
        if (prevChallenge) {
            return (
                <Link className="nav-btn d-flex center" to={`/challenge/${prevChallenge}`}>
                    <ArrowBack />
                    <span>Prev</span>
                </Link>
            );
        }

        return <div />;
    }

    const ChallengeNavigation = (): JSX.Element => (
        <div className="challenge-navigation d-flex spaced center">
            <PrevChallenge />
            <NextChallenge />
        </div>
    );

    const BackButton = () => (
        <Link className="nav-btn back-btn mb-30 d-flex center" to={`/domain/${domainId}`}>
            <ArrowBack />
            <span>{domain}</span>
        </Link>
    );

    const answerAndChallengeLoading = challengeLoading && answerLoading;

    if (!challengeExists) {
        return <ChallengeDoesNotExist />
    }

    return (
        <div className="container pt-40">
            <div className="content">
                <div>
                    <Tile loading={answerAndChallengeLoading} type="status">
                        <BackButton />
                    </Tile>
                    <Tile loading={answerAndChallengeLoading} type="status">
                        <Status
                            defaultLabel="unanswered"
                            label={`${status} understanding`}
                            status={resultStatus}
                        />
                    </Tile>
                    <Tile loading={answerAndChallengeLoading} type="title">
                        <h2>{challenge}</h2>
                    </Tile>
                </div>
                <FormLoader loading={answerAndChallengeLoading}>
                    <form onSubmit={onSubmit}>
                        <FormGroup>
                            <LabelWithCounter
                                maxLength={1000}
                                value={answer}
                            >
                                Your answer
                            </LabelWithCounter>
                            <AnswerTextArea
                                setAnswer={setAnswer}
                                answer={answer}
                                loading={loading}
                            />
                            <ChallengeNavigation />
                            <ErrorMessage
                                message={error}
                            />
                        </FormGroup>
                    </form>
                </FormLoader>
                <ContestChallenge
                    answer={answer}
                    challengeId={challengeId}
                    visible={showContestAlert}
                />
            </div>
        </div>
    );
};

const AnswerTextArea = (props: any): JSX.Element => {
    const { setAnswer, answer, loading } = props;
    const onChange = (e: any): void => setAnswer(e.target.value);

    return (
        <div className="answer-text-area text-area-shadow form-control-rounded">
            <textarea
                className="form-control border-none"
                onChange={onChange}
                maxLength={1000}
                rows={8}
                value={answer}
            ></textarea>
            <div className="answer-form-footer d-flex justify-content-center">
                <Button
                    className="btn btn-default answer-btn"
                    loading={loading}
                >
                    Submit
                </Button>
            </div>
        </div>
    );
}

const ContestChallenge = (props: ContestChallengeProps) => {
    const [visible, setVisibilty] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const [sentContestion, setSentContestion] = useState<boolean>(false);

    useEffect(() => {
        setVisibilty(props.visible);
    }, [props.visible]);


    const submitContestion = (e: any): void => {
        e.preventDefault();

        setLoading(true);

        const headers = {
            'Authorization': `Bearer ${window.localStorage.getItem('token')}`,
        };

        const { answer, challengeId } = props;
        const data = {
            answer,
            challenge_id: challengeId,
        }

        axios.post(`${API_URL}/challenge/contest`, data, { headers })
            .then(() => {
                setSentContestion(true);
                setLoading(false);
            })
            .catch((error) => {
                console.error(error);
                setLoading(false);
            });
    }

    const override = `
        padding: 1px;
        display: flex;
        justify-content: center;
        align-items: center;
    `;

    if (!visible) {
        return <></>;
    }

    if (loading) {
        return (
            <div className="alert alert-warning">
                <BeatLoader css={override} color="#fff" sizeUnit="px" size={10} />
            </div>
        );
    }

    if (sentContestion) {
        return (
            <div className="alert alert-success d-flex center">
                <div className="alert-icon d-flex center">
                    <CheckCircleOutlined />
                </div>
                <span>Request sent</span>
            </div>
        );
    }

    return (
        <div className="alert alert-warning d-flex center">
            <span>
                Think you have the right answer? <b className="hover" onClick={submitContestion}>Let us know!</b>
            </span>
        </div>
    );
};

interface ContestChallengeProps {
    answer: string;
    challengeId: string;
    visible: boolean;
}

export default withRouter(AnswerChallenge);
