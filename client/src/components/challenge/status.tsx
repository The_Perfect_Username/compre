import React from 'react';

const Status = (props: statusProps) => {
    const {
        defaultLabel,
        label,
        status,
    }: statusProps = props;

    const rules: Rules = {
        weak: 'danger',
        good: 'pending',
        strong: 'success',
        published: 'default',
        submitted: 'pending',
        modified: 'pending',
        demo: 'default',
        default: 'default',
    };

    const hasStatus = rules && rules.hasOwnProperty(status);

    const statusClass = rules[status || 'default'];
    const currentLabel = hasStatus ? label : defaultLabel;

    return (
        <span className={`status ${statusClass}`}>{currentLabel}</span>
    );
};

interface Rules {
    weak: string;
    good: string;
    strong: string;
    published: string;
    submitted: string;
    demo: string;
    default: string;
    modified: string;
}

type statusProps = {
    defaultLabel?: string;
    label: string;
    status: StatusLabels;
};

export type StatusLabels = 'weak' | 'good' | 'strong' | 'published' | 'submitted' | 'demo' | '';

export default Status;